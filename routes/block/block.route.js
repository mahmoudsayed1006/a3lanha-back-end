import express from 'express';
import blockController from '../../controllers/block/block.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:blockedId/block')
    .post(
        requireAuth,
        blockController.create
    );
router.route('/')
    .get(requireAuth,blockController.findAll);
    
router.route('/:blockedId/unblock')
    .delete( requireAuth,blockController.unblock);







export default router;