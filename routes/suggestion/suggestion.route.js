import express from 'express';
import SuggestionController from '../../controllers/suggestion/suggestion.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        //requireAuth,
        SuggestionController.validateBody(),
        SuggestionController.create
    )
router.route('/')
    .get(SuggestionController.findAll);
    
router.route('/:SuggestionId')
    .put(
        requireAuth,
        SuggestionController.validateBody(true),
        SuggestionController.update
    )
    .delete( requireAuth,SuggestionController.delete);

export default router;