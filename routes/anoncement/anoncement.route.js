import express from 'express';
import AnoncementController from '../../controllers/anoncement/anoncement.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('anoncement').single('img'),
        AnoncementController.validateBody(),
        AnoncementController.create
    )
    .get(AnoncementController.findAll);
    
router.route('/:anonId')
    .put(
        requireAuth,
        multerSaveTo('anoncement').single('img'),
        AnoncementController.validateBody(true),
        AnoncementController.update
    )
    .get(AnoncementController.findById)
    .delete( requireAuth,AnoncementController.delete);




export default router;