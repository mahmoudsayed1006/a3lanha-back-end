import express from 'express';
import { requireSignIn, requireAuth,instaSignIn} from '../../services/passport';
const passport = require('passport');
import UserController from '../../controllers/user/user.controller';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();


router.post('/signin', requireSignIn, UserController.signIn);
//new login first step
router.route('/login')
    .post(
        UserController.validateFirstStepBody(),
        UserController.LoginFirstStep
    );
router.route('/:userId/completeSignUp')
    .put(
        multerSaveTo('users').single('img'),
        UserController.validateComplateDataBody(true),
        UserController.completeSignUp
    );

router.route('/signup')
    .post(
        multerSaveTo('users').single('img'),
        UserController.validateUserCreateBody(),
        UserController.signUp
    );

router.route('/:userId/block')
    .put(
        requireAuth,
        UserController.block
    );

router.route('/:userId/unblock')
    .put(
        requireAuth,
        UserController.unblock
    );
router.route('/:userId/enableComplaint')
    .put(
        requireAuth,
        UserController.enableComplaint
    );

router.route('/:userId/disableComplaint')
    .put(
        requireAuth,
        UserController.disableComplaint
    );
router.route('/enableNotif')
    .put(
        requireAuth,
        UserController.enableNotif
    );

router.route('/disableNotif')
    .put(
        requireAuth,
        UserController.disableNotif
    );
router.route('/logout')
    .post(
        requireAuth,
        UserController.logout
    );
router.route('/addToken')
    .post(
        requireAuth,
        UserController.addToken
    );
router.route('/updateToken')
    .put(
        requireAuth,
        UserController.updateToken
    );


router.put('/check-exist-email', UserController.checkExistEmail);

router.put('/check-exist-phone', UserController.checkExistPhone);

router.put('/user/:userId/updateInfo',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.validateUpdatedBody(true),
    UserController.updateInfo);

router.put('/user/:userId/updateAvatar',
    requireAuth,
    multerSaveTo('users').single('img'),
    UserController.updateAvatar);

router.put('/user/updatePassword',
    requireAuth,
    UserController.validateUpdatedPassword(),
    UserController.updatePassword);

router.post('/sendCode',
    UserController.validateSendCode(),
    UserController.sendCodeToEmail);

router.post('/confirm-code',
    UserController.validateConfirmVerifyCode(),
    UserController.resetPasswordConfirmVerifyCode);

router.post('/reset-password',
    UserController.validateResetPassword(),
    UserController.resetPassword);

router.route('/getAll')
    .get(UserController.findAll)

router.route('/searchUser')
    .put(UserController.searchUser)  

router.route('/:userId/getUser')
    .get(UserController.getUser)
router.route('/:userId/getUserWithToken')
    .get(requireAuth,UserController.getUserWithToken)

router.route('/:userId/delete')
    .delete(requireAuth,UserController.delete)

router.post('/socialLogin', UserController.googleLogin);

router.get('/auth/instagram',instaSignIn);

router.get('/auth/instagram/callback', 
       instaSignIn,UserController.socialLogin);

router.get('/auth/facebook',
        passport.authenticate('facebook',{
        scope:  [ 'email', 'public_profile', 'user_photos']
}));
 
router.get('/auth/facebook/callback',
  passport.authenticate('facebook'),UserController.socialLogin);


router.route('/:userId/blockPeople')
  .put(requireAuth,UserController.blockPeople)
router.route('/:userId/unblockPeople')
  .put(requireAuth,UserController.unblockPeople)

router.route('/countryName')
    .post(
        
        UserController.countryName
    );
    //update email
router.route('/checkPassword')
    .post(
        requireAuth,
        UserController.validateCheckPassword(),
        UserController.checkPassword
    );
router.route('/sendCode-updateEmail')
    .post(
        requireAuth,
        UserController.validateSendCodeUpdateEmail(),
        UserController.sendCode
    );
router.route('/updateEmail')
    .put(
        requireAuth,
        UserController.validateUpdateEmail(),
        UserController.updateEmail
    );
export default router;
