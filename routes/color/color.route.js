
import express from 'express';
import ColorController from '../../controllers/color/color.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('Colors').single('img'),
        ColorController.validateBody(),
        ColorController.create
    )
    .get(ColorController.findAll);
router.route('/withoutPagenation/get')
    .get(ColorController.findAllWithoutPagenation);
router.route('/:ColorId')
    .put(
        requireAuth,
        multerSaveTo('Colors').single('img'),
        ColorController.validateBody(true),
        ColorController.update
    )
    .get(ColorController.findById)
    .delete(requireAuth,ColorController.delete);






export default router;
