import express from 'express';
import AdsController from '../../controllers/ads/ads.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';
 
const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('ads').array('img',10),
        AdsController.validateBody(),
        AdsController.create
    )
    .get(AdsController.findAll);
router.route('/withToken/get')
    .get(requireAuth,AdsController.findAllWithToken);

router.route('/:adsId/withToken/get')
    .get(requireAuth,AdsController.findByIdWithToken);

router.route('/:adsId/findAllParticipates')
    .get(requireAuth,AdsController.findAllParticipates);
router.route('/:adsId')
    .put(
        requireAuth,
        multerSaveTo('ads').fields([
            { name: 'img', maxCount: 10, options: false }
        ]),
        AdsController.validateBody(true),
        AdsController.update
    )
    .get(AdsController.findById)
    .delete( requireAuth,AdsController.delete);
    
router.route('/:adsId/private')
    .put(
        requireAuth,
        AdsController.privateAdd
    )

router.route('/:adsId/active')
    .put(
        requireAuth,
        AdsController.active
    )

router.route('/:adsId/dis-active')
    .put(
        requireAuth,
        AdsController.disactive 
    )

router.route('/:adsId/top')
    .put(
        requireAuth,
        AdsController.top
    )

router.route('/:adsId/low')
    .put(
        requireAuth,
        AdsController.low
    )
router.route('/:adsId/enableNotif')
    .put(
        requireAuth,
        AdsController.enableNotif
    )

router.route('/:adsId/disableNotif')
    .put(
        requireAuth,
        AdsController.disableNotif
    )
router.route('/:adsId/soled')
    .put(
        requireAuth,
        AdsController.soled
    )

router.route('/:adsId/unSoled')
    .put(
        requireAuth,
        AdsController.unsoled
    )
router.route('/:adsId/rate')
    .put(
        requireAuth,
        AdsController.ratValidateBody(),
        AdsController.rate
    )
router.route('/:adsId/findAdsRate')
    .get(
        AdsController.findAllRate
    )
router.route('/search/ads')
    .get(
        AdsController.search
    )
router.route('/search/ads/withToken')
    .get(
        requireAuth,
        AdsController.searchWithToken
    )
router.route('/shareInside/:adsId')
    .post(
        requireAuth,
        AdsController.shareInside
    )
router.route('/share')
    .post(
        requireAuth,
        AdsController.shareValidateBody(),
        AdsController.share
    )

router.route('/share/:adsId')
    .post(
        requireAuth,
        AdsController.shareValidateBody(),
        AdsController.adshare
    )
export default router;