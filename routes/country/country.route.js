import express from 'express';
import CountryController from '../../controllers/country/country.controller';
import CityController from '../../controllers/city/city.controller';
import { multerSaveTo } from '../../services/multer-service';
import { requireAuth } from '../../services/passport';

const router = express.Router();


router.route('/')
    .post(
        requireAuth,
        multerSaveTo('country').single('img'),
        CountryController.validateCountryBody(),
        CountryController.create
    )
    .get(CountryController.getAllPaginated);
router.route('/withoutPagenation/get')
    .get(CountryController.getAll);
router.route('/:countryId')
    .put(
        requireAuth,
        multerSaveTo('country').single('img'),
        CountryController.validateCountryBody(true),
        CountryController.update
    )
    .get(CountryController.getById)
    .delete(requireAuth,CountryController.delete);

router.route('/:countryId/cities')
    .post(
        requireAuth,
        CityController.validateCityBody(),
        CityController.create
    )
    .get(CityController.getAllPaginated);
router.route('/:countryId/cities/withoutPagenation')
    .get(CityController.getAll);

router.route('/cities/:cityId')
    .put(
        requireAuth,
        CityController.validateCityBody(true),
        CityController.update
    )
    .get(CityController.getById)
    .delete(requireAuth,CityController.delete);




export default router;