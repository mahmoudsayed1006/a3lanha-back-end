import express from 'express';
import SliderController from '../../controllers/slider/slider.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';
 
const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('Slider').array('img',10),
        SliderController.validateBody(),
        SliderController.create
    )
    .get(SliderController.getAll);
    
router.route('/:SliderId')
    .put(
        requireAuth,
        multerSaveTo('Slider').fields([
            { name: 'img', maxCount: 10, options: false }
        ]),
        SliderController.validateBody(true),
        SliderController.update
    )
    .delete( requireAuth,SliderController.delete);

export default router;