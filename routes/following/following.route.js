import express from 'express';
import FollowingController from '../../controllers/following/following.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:followingId/follow')
    .post(
        requireAuth,
        FollowingController.create
    );
router.route('/')
    .get(requireAuth,FollowingController.findAll);
    
router.route('/:followingId/unfollow')
    .delete( requireAuth,FollowingController.unfollow);







export default router;