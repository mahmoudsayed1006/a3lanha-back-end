import express from 'express';
import userRoute from './user/user.route';
import CategoryRoute  from './category/category.route';
import messageRoute  from './message/message.route';
import ContactRoute  from './contact/contact.route';
import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import AdminRoute  from './admin/admin.route';
import AboutRoute  from './about/about.route';
import AnoncementRoute  from './anoncement/anoncement.route';
import countryRoute  from './country/country.route';
import FollowingRoute  from './following/following.route';
import AdsRoute  from './ads/ads.route';
import FavouriteRoute  from './favourite/favourite.route';
import OptionRoute  from './option/option.route';
import SliderRoute  from './slider/slider.route';
import ModalRoute  from './models/models.route';
import BrandRoute  from './brand/brand.route';
import ColorRoute  from './color/color.route';
import ProblemRoute  from './problem/problem.route';
import SuggestionRoute  from './suggestion/suggestion.route';
import BlockRoute  from './block/block.route';

import { requireAuth } from '../services/passport';

const router = express.Router();

router.use('/', userRoute);


router.use('/categories',CategoryRoute);
router.use('/follow',FollowingRoute);
router.use('/favourite', FavouriteRoute);
router.use('/block', BlockRoute);

router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);

router.use('/notif',requireAuth, NotifRoute);
router.use('/admin',requireAuth, AdminRoute);
router.use('/about',AboutRoute);
router.use('/option',OptionRoute);

router.use('/anoncement',AnoncementRoute);
router.use('/messages',messageRoute);
router.use('/ads', AdsRoute);

router.use('/countries', countryRoute);
router.use('/slider', SliderRoute);
router.use('/brand', BrandRoute);
router.use('/color', ColorRoute);

router.use('/model', ModalRoute);
router.use('/problem', ProblemRoute);
router.use('/suggestion', SuggestionRoute);
export default router;
