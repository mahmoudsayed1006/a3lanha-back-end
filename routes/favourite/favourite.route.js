import express from 'express';
import FavouriteController from '../../controllers/favourite/favourite.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/:adsId/add')
    .post(
        requireAuth,
        FavouriteController.create
    );
router.route('/')
    .get(requireAuth,FavouriteController.findAll);
    
router.route('/:adsId')
    .delete( requireAuth,FavouriteController.unFavourite);







export default router;