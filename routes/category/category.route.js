import express from 'express';
import CategoryController from '../../controllers/category/category.controller';
import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';


const router = express.Router();


router.get('/:categoryId/sub-categories', CategoryController.findSubCategory);
router.get('/sub-categories', CategoryController.findAllSubCategory);
router.get('/:categoryId/sub-categories/withoutPagenation/get', CategoryController.findSubCategoryWithoutPagenation);
router.get('/withoutPagenation/get', CategoryController.findAllCategoryWithoutPagenation);
router.get('/select-categories', CategoryController.findSelection);

router.route('/:categoryId')
    .put(
        requireAuth,
        multerSaveTo('categories').single('img'),
        CategoryController.validateBody(true),
        CategoryController.update
    )
    .get(CategoryController.findById)
    .delete(CategoryController.delete);


router.route('/')
    .post(
        requireAuth,
        multerSaveTo('categories').single('img'),
        CategoryController.validateBody(),
        CategoryController.create
    )
    .get(CategoryController.findCategory);

router.route('/:categoryId/trend')
    .put(
        requireAuth,
        CategoryController.trend
    );
router.route('/:categoryId/untrend')
    .put(
        requireAuth,
        CategoryController.untrend
    );



export default router;
