import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import {handleImgs, handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Ads from "../../models/ads/ads.model";
import Option from "../../models/option/option.model";

import User from "../../models/user/user.model";
import { sendEmail } from "../../services/emailMessage.service";
import { toImgUrl } from "../../utils";

import Notif from "../../models/notif/notif.model";
import Rate from "../../models/rate/rate.model";
import Favourite from "../../models/favourite/favourite.model";
import Problem from "../../models/problem/problem.model";

import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const populateQuery = [
    { path: 'country', model: 'country' },
    { path: 'category', model: 'category' },
    { path: 'subCategory', model: 'category' },
    { path: 'owner', model: 'user' },
    { path: 'color', model: 'color' },
    { path: 'brand', model: 'brand' },
    { path: 'modal', model: 'model' },
    { path: 'city', model: 'city' },
    { path: 'viewParticipates', model: 'user' },
    { path: 'participate', model: 'user' },

    
];
const populateQuery2 = [
    { path: 'user', model: 'user' }
];
const populateQuery3 = [ 
    { path: 'countryId', model: 'country' },
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {year,rate,onlyMe,active,price,modal,top,owner,sortByPrice,category,subCategory,country,city,address,priceFrom,priceTo,color,brand,rental,beds,baths,space,usage,jopType,jopRequirements,salarySystem} = req.query;
            let query = {deleted: false};
           
 
            if(priceFrom && priceTo == undefined) {
                query = {
                    $and: [
                        { $or: [
                           { price : { $gte : priceFrom}}, 
                           { priceFrom : { $gte : priceFrom}}, 
                          
                          ] 
                        },
                        {deleted: false} 
                    ]
                }; 
               
            } 
            if(priceTo && priceFrom == undefined) {
                query = {
                    $and: [
                        { $or: [
                            {price : {$lte : priceTo }},
                            {priceTo : {$lte : priceTo }}, 
                          
                          ] 
                        },
                        {deleted: false} 
                    ]
                };
            } 
            if(priceTo && priceFrom) {
                query = {
                    $and: [
                        { $or: [
                            {price : {$gte : priceFrom , $lte : priceTo }},
                            {$and: [
                                {priceTo : {$lte : priceTo }}, 
                                {priceFrom : { $gte : priceFrom}}, 
                            ]}
                          ] 
                        },
                        {deleted: false} 
                    ]
                };
            } 
            
            if(address){
               query.address = { $regex: '.*' + address + '.*' , '$options' : 'i'  }
            }    
            if (top) query.top = top;
            if (rate) query.finalRate = rate;
            if (active) query.visible = active;
            if (onlyMe == 'true') query.onlyMe = true;
            if (onlyMe == 'false') query.onlyMe = false;
            if (country) query.country = country;
            if (city) query.city = city;
            if (owner) query.owner = owner;
            if (category) query.category = category;
            if (subCategory) query.subCategory = subCategory;
            if (color) query.color = color;
            if (brand) query.brand = brand;
            if (modal) query.modal = modal;
            if (rental) query.rental = rental;
            if (beds) query.beds = beds;
            if (baths) query.baths = baths;
            if (space) query.space = space;
            if (usage) query.usage = usage;
            if (year) query.year = year;
            if (jopType) query.jopType = jopType;
            if(salarySystem) query.salarySystem = salarySystem;
            if(jopRequirements){
                query.jopRequirements = { $regex: '.*' + jopRequirements + '.*' }
             }
            let sort = {createdAt: -1}
            if (sortByPrice) sort = {price:1};
            let ads = await Ads.find(query).populate(populateQuery)
                .sort(sort)
                .limit(limit)
                .skip((page - 1) * limit);
            for (let add of ads) {
                let theAds = await checkExistThenGet(add._id,Ads)
                theAds.isFavourite =false
                theAds.viewParticipates= [...theAds.participate.slice(0,5)]
                await theAds.save()
            }
            ads = await Ads.find(query).populate(populateQuery)
                .sort(sort)
                .limit(limit)
                .skip((page - 1) * limit);
            const adsCount = await Ads.count(query);
            const pageCount = Math.ceil(adsCount / limit);

            res.send(new ApiResponse(ads, page, pageCount, limit, adsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllWithToken(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {year,rate,onlyMe,active,price,modal,top,owner,sortByPrice,category,subCategory,country,city,address,priceFrom,priceTo,color,brand,rental,beds,baths,space,usage,jopType,jopRequirements,salarySystem} = req.query;
            let query = {deleted: false};
           
 
            if(priceFrom && priceTo == undefined) {
                query = {
                    $and: [
                        { $or: [
                           { price : { $gte : priceFrom}}, 
                           { priceFrom : { $gte : priceFrom}}, 
                          
                          ] 
                        },
                        {deleted: false} 
                    ]
                }; 
               
            } 
            if(priceTo && priceFrom == undefined) {
                query = {
                    $and: [
                        { $or: [
                            {price : {$lte : priceTo }},
                            {priceTo : {$lte : priceTo }}, 
                          
                          ] 
                        },
                        {deleted: false} 
                    ]
                };
            } 
            if(priceTo && priceFrom) {
                query = {
                    $and: [
                        { $or: [
                            {price : {$gte : priceFrom , $lte : priceTo }},
                            {$and: [
                                {priceTo : {$lte : priceTo }}, 
                                {priceFrom : { $gte : priceFrom}}, 
                            ]}
                          ] 
                        },
                        {deleted: false} 
                    ]
                };
            } 
            
            if(address){
               query.address = { $regex: '.*' + address + '.*' , '$options' : 'i'  }
            }    
            if (year) query.year = year;
            if (top) query.top = top;
            if (rate) query.finalRate = rate;
            if (active) query.visible = active;
            if (onlyMe == 'true') query.onlyMe = true;
            if (onlyMe == 'false') query.onlyMe = false;
            if (country) query.country = country;
            if (city) query.city = city;
            if (owner) query.owner = owner;
            if (category) query.category = category;
            if (subCategory) query.subCategory = subCategory;
            if (color) query.color = color;
            if (brand) query.brand = brand;
            if (modal) query.modal = modal;
            if (rental) query.rental = rental;
            if (beds) query.beds = beds;
            if (baths) query.baths = baths;
            if (space) query.space = space;
            if (usage) query.usage = usage;
            if (jopType) query.jopType = jopType;
            if(salarySystem) query.salarySystem = salarySystem;
            if(jopRequirements){
                query.jopRequirements = { $regex: '.*' + jopRequirements + '.*' }
             }
            let sort = {createdAt: -1}
            if (sortByPrice) sort = {price:1};
            let ads = await Ads.find(query)
                .sort(sort)
                .limit(limit)
                .skip((page - 1) * limit);
            let theUser = await checkExistThenGet(req.user._id,User)
            let data = []
            for (let add of ads) {
                let theAds = await checkExistThenGet(add._id,Ads)
                theAds.viewParticipates= [...theAds.participate.slice(0,5)]
                let arr = theUser.favourite
                var found = arr.find(function(element) {
                    return element == add._id;
                }); 
                if(found){
                    theAds.isFavourite =true
                }else{
                    theAds.isFavourite =false
                }
                await theAds.save();
                let found2 = theUser.blocked.find(e => e == theAds.owner)
                if(!found2){
                    data.push(theAds.id)
                }
            }
            ads = await Ads.find({'_id':data}).populate(populateQuery)
                .sort(sort)
                .limit(limit)
                .skip((page - 1) * limit);
            const adsCount = await Ads.count({'_id':data});
            const pageCount = Math.ceil(adsCount / limit);

            res.send(new ApiResponse(ads, page, pageCount, limit, adsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findSelection(req, res, next) {
        try {
            let query = { deleted: false };
            let ads = await Ads.find(query)
                .sort({ createdAt: -1 });
            res.send(ads)
        } catch (err) {
            next(err);
        }
    },
    async findAllParticipates(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {adsId} = req.params;
            let participates = await Ads.findById(adsId).distinct("participate")
            let query = {
                deleted: false,
                _id:participates
            }
            let users = await User.find(query).populate(populateQuery3)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            for (let user of users) {
                let theUser = await checkExistThenGet(user._id, User)
                let arr = myUser.following;
                var found = arr.find(function(element) {
                    return element == user._id;
                });
                if(found){
                    theUser.isFollowing = true
                } else{
                    theUser.isFollowing = false
                }
                await theUser.save()
            }
            
            users = await User.find(query).populate(populateQuery3)
                .sort(sortd)
                .limit(limit)
                .skip((page - 1) * limit);

            const usersCount = await User.count(query);
            const pageCount = Math.ceil(usersCount / limit);

            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    },
    validateBody(isUpdate = false) {
        let validations = [
            body('description').not().isEmpty().withMessage('description is required'),
            body('title').not().isEmpty().withMessage('title is required'),
            body('price').optional(),
            body('address').not().isEmpty().withMessage('address is required'),
            body('email').not().isEmpty().withMessage('email is required'),
            body('phone').not().isEmpty().withMessage('phone is required'),
            body('category').not().isEmpty().withMessage('category is required')
            .isNumeric().withMessage('category numeric value required'),
            body('subCategory').not().isEmpty().withMessage('subCategory is required')
            .isNumeric().withMessage(' subCategory numeric value required'),
            body('country').not().isEmpty().withMessage('country is required')
            .isNumeric().withMessage('country numeric value required'),
            body('city').not().isEmpty().withMessage('city is required')
            .isNumeric().withMessage('city numeric value required'),
            body('participate').optional(),
            body('priceFrom').optional(),
            body('priceTo').optional(),
            body('color').optional().isNumeric().withMessage('category numeric value required'),
            body('brand').optional(),
            body('usage').optional(),
            body('companyName').optional(),
            body('jopType').optional(),
            body('jopRequirements').optional(),
            body('experience').optional(),
            body('salarySystem').optional(),
            body('date').optional(),
            body('modal').optional(),
            body('year').optional(),
            body('mileage').optional(),
            body('rental').optional(),
            body('beds').optional(),
            body('baths').optional(),
            body('space').optional(),
            body('goal').optional(),
            body('invitationCount').optional(),
            body('lat').optional(),
            body('long').optional(),
            body('personal').optional(),
            
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },

    async create(req, res, next) {

        try { 
            let user = req.user;
            let owner = await checkExistThenGet(req.user._id, User, { deleted: false });

            if(owner.block == true){
                return next(new ApiError(403, ('sorry you are blocked')));
            }
            const validatedBody = checkValidations(req);
            if(validatedBody.lat && validatedBody.long){
                validatedBody.location = [validatedBody.lat,validatedBody.long]
            }
            validatedBody.owner = req.user._id;
            console.log(req.files)
            if(validatedBody.personal== 'true'){
                validatedBody.personal = true
            }
            if(validatedBody.personal== 'false'){
                validatedBody.personal = false
            }
            if(req.files.length > 0){
                let images = await handleImgs(req);
                validatedBody.img = images
            }
            let option = await Option.find({ deleted: false }).select("enable");
            console.log(option)
            let optionValue = option[0].enable;
            let createdAds;
            if(optionValue == false){
                validatedBody.visible = false;
                createdAds = await Ads.create({ ...validatedBody});
                let admins = await User.find({type:'ADMIN'});
                admins.forEach(user => {
                    sendNotifiAndPushNotifi({
                        targetUser: user.id, 
                        fromUser: req.user._id, 
                        text: 'new notification',
                        subject: createdAds.id,
                        subjectType: req.user.username + ' add new advertisement'
                    });
                    let notif = {
                        "description":req.user.username + ' add new advertisement',
                        "arabicDescription":req.user.username + " تم اضافه اعلان بواسطه" 
                    }
                    Notif.create({...notif,resource:req.user._id,target:user.id,ads:createdAds.id});
                });
            } else{
                validatedBody.visible = true;
                createdAds = await Ads.create({ ...validatedBody});
               
            }
            
            let users = await User.find({_id:validatedBody.participate,notif:true});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: user.id,
                    subjectType: req.user.username + ' add new advertisement'
                });
                let notif = {
                    "description":req.user.username + ' add new advertisement',
                    "arabicDescription":req.user.username + " تم اضافه اعلان بواسطه" 
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,ads:createdAds.id});
            });
            let reports = {
                "action":"Create Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdAds);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { adsId } = req.params;
            await checkExist(adsId, Ads, { deleted: false });
            let theAds = await checkExistThenGet(adsId,Ads)
            theAds.viewParticipates= [...theAds.participate.slice(0,5)]
            theAds.isFavourite =false
            await theAds.save()
            let ads = await Ads.findById(adsId).populate(populateQuery)
            res.send(ads);
        } catch (err) {
            next(err);
        }
    },
    async findByIdWithToken(req, res, next) {
        try {
            let { adsId } = req.params;
            let theAds = await checkExistThenGet(adsId, Ads, { deleted: false });
            let theUser = await checkExistThenGet(req.user._id,User)
            let arr3 = theUser.blocked
            var found3 = arr3.find(function(element) {
                return element == theAds.owner;
            }); 
            if(found3){
                return next(new ApiError(500, ('you block the ads owner')));
            }
           
            let arr = theUser.favourite
            var found = arr.find(function(element) {
                return element == adsId;
            }); 
            if(found){
                theAds.isFavourite =true
            }else{
                theAds.isFavourite =false
            }
            theAds.viewParticipates= [...theAds.participate.slice(0,5)]
            await theAds.save()
            let owner = theAds.owner;
            let theOwner = await checkExistThenGet(owner,User)
            let arr2 = theUser.following
            var found2 = arr2.find(function(element) {
                return element == owner;
            }); 
            if(found2){
                theOwner.isFollowing =true
            }else{
                theOwner.isFollowing =false
            }
            await theOwner.save()
            let ads = await Ads.findById(adsId).populate(populateQuery)
            res.send(ads);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            
            let { adsId } = req.params;
            let myAdd = await checkExistThenGet(adsId, Ads, { deleted: false });

            if (req.user._id != myAdd.owner)
            return next(new ApiError(403, ('admin.auth')));
            
            const validatedBody = checkValidations(req);
             if(validatedBody.lat && validatedBody.long){
                validatedBody.location = [validatedBody.lat,validatedBody.long]
            }
            console.log(validatedBody)
            if (req.files) {
                if (req.files['img']) {
                    let imagesList = [];
                    for (let imges of req.files['img']) {
                        imagesList.push(await toImgUrl(imges))
                    }
                    validatedBody.img = imagesList;
                }
            }
            validatedBody.onlyMe = false;
            let updatedAds = await Ads.findByIdAndUpdate(adsId, {
                ...validatedBody
            });
            let reports = {
                "action":"Update Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedAds);
        }
        catch (err) {
            next(err);
        }
    },
    async top(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {adsId} = req.params;
            let ads = await checkExistThenGet(adsId, Ads,
                {deleted: false });
            ads.top = true;
            await ads.save();
            let reports = {
                "action":"Top ads",
            };
            let report = await Report.create({...reports, user: user });
        
            res.send('ads become top');
            
        } catch (error) {
            next(error);
        }
    },

    async low(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 

            let {adsId } = req.params;
            let ads = await checkExistThenGet(adsId, Ads,
                {deleted: false });

            ads.top = false;
            await ads.save();
            let reports = {
                "action":"Low ads",
            };
            let report = await Report.create({...reports, user: user });
            res.send('ads low');
        } catch (error) {
            next(error);
        }
    },
    async active(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let {adsId} = req.params;
            let ads = await checkExistThenGet(adsId, Ads,
                {deleted: false });
            ads.visible = true;
            await ads.save();
            let reports = {
                "action":"Active ads",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: ads.owner, 
                fromUser: req.user._id, 
                text: 'new notification',
                subject: adsId,
                subjectType: ' your advertisement has been accepted'
            });
            let notif = {
                "description":' your advertisement has been accepted',
                "arabicDescription":" تم قبول اعلانك"
            }
            Notif.create({...notif,resource:req.user._id,target:ads.owner,ads:adsId});
            res.send('ads active');
            
        } catch (error) {
            next(error);
        }
    },

    async disactive(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth'))); 

            let {adsId } = req.params;
            let ads = await checkExistThenGet(adsId, Ads,
                {deleted: false });

            ads.visible = false;
            await ads.save();
            let reports = {
                "action":"Dis-active ads",
            };
            let report = await Report.create({...reports, user: user });
            sendNotifiAndPushNotifi({
                targetUser: ads.owner, 
                fromUser: req.user._id, 
                text: 'new notification',
                subject: adsId,
                subjectType: ' your advertisement has been refused'
            });
            let notif = {
                "description":' your advertisement has been refused',
                "arabicDescription":" تم رفض اعلانك"
            }
            Notif.create({...notif,resource:req.user._id,target:ads.owner,ads:adsId});
            res.send('ads dis-active');
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        try {
            let user = req.user;
                
            let { adsId } = req.params;
            let ads = await checkExistThenGet(adsId, Ads, { deleted: false });
            
            ads.deleted = true;
            //remove notifs
            let notifs = await Notif.find({deleted: false,ads: adsId});
            for (let id2 of notifs) {
                id2.deleted = true;
                await id2.save();
            }
            //remove favs
            let favs = await Favourite.find({delete: false,ads: adsId});
            for (let id3 of favs) {
                id3.deleted = true;
                await id3.save();
            }
            //remove rates
            let rates = await Rate.find({delete: false,ads: adsId});
            for (let id4 of rates) {
                id4.deleted = true;
                await id4.save();
            }
            //remove problems
            let problems = await Problem.find({delete: false,ads: adsId});
            for (let id5 of problems) {
                id5.deleted = true;
                await id5.save();
            }
            await ads.save();
            let reports = {
                "action":"Delete Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    async privateAdd(req, res, next) {
        try {
            let user = req.user;
                
            let { adsId } = req.params;
            let ads = await checkExistThenGet(adsId, Ads, { deleted: false });
            
            ads.onlyMe = true;
            await ads.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    ratValidateBody(isUpdate = false) {
        let validations = [
            body('comment').not().isEmpty().withMessage('comment is required'),
            body('rate').not().isEmpty().withMessage('rate is required')
            .isNumeric().withMessage('rate numeric value required'),
            
        ];
        return validations;
    },
    async rate(req, res, next) {
        try {
            let user = req.user;
            let { adsId } = req.params;
            let addUser = await checkExistThenGet(req.user._id, User, { deleted: false });
            let ads = await checkExistThenGet(adsId, Ads, { deleted: false });
            const validatedBody = checkValidations(req);
            validatedBody.ads = adsId;
            validatedBody.user = req.user._id;
            await Rate.create({ ...validatedBody });

            //rate
            let newRate = ads.rate + parseInt(validatedBody.rate);
            ads.rate = newRate;
            ads.rateNumbers = ads.rateNumbers + 1;
            let totalDegree = ads.rateNumbers * 5; //عدد التاسكات فى الدرجه النهائيه
            let degree = newRate * 100
            ads.ratePrecent = degree / totalDegree;
            console.log(ads.ratePrecent)
            let x = ads.ratePrecent;
            let rateFrom5 = x / 20
            console.log(x)
            ads.finalRate = ads.finalRate + Math.ceil(parseInt(rateFrom5));
            console.log( ads.finalRate)
            if(validatedBody.rate == "1"){
                ads.star1 =ads.star1 + 1;
            }
            if(validatedBody.rate == "2"){
                ads.star2 =ads.star2 + 1;
            }
            if(validatedBody.rate == "3"){
                ads.star3 =ads.star3 + 1;
            }
            if(validatedBody.rate == "4"){
                ads.star4 =ads.star4 + 1;
            }
            if(validatedBody.rate == "5"){
                ads.star5 =ads.star5 + 1;
            }
            await ads.save();
            if(addUser.notif == true && ads.notif == true){
                sendNotifiAndPushNotifi({
                    targetUser: ads.owner, 
                    fromUser: req.user, 
                    text: 'new notification',
                    subject: ads.id,
                    subjectType: req.user.username + ' add comment like your advertisement'
                });
                let notif = {
                    "description":req.user.username + ' add comment your advertisement',
                    "arabicDescription":'  بعمل  تعليق على اعلانك'+ req.user.username +' قام'
                }
                await Notif.create({...notif,resource:req.user,target:ads.owner,ads:adsId});
            }
            res.status(204).send(ads);

        }
        catch (err) {
            next(err);
        }
    },
    async findAllRate(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { adsId } = req.params;
            let query = {deleted: false,ads:adsId };
 
            let adsRate = await Rate.find(query).populate(populateQuery2)
                .sort({createdAt: -1})
                .limit(limit)
                .skip((page - 1) * limit);


            const adsCount = await Rate.count(query);
            const pageCount = Math.ceil(adsCount / limit);

            res.send(new ApiResponse(adsRate, page, pageCount, limit, adsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async search(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {search,category,subCategory,country,active,onlyMe} = req.query;
            
            let query = {
                $and: [
                    { $or: [
                        {title: { $regex: '.*' + search + '.*' , '$options' : 'i'  }}, 
                      
                      ] 
                    },
                    {deleted: false},
                    {onlyMe:false}
                ]
            };
            if (country) query.country = country;
            if (category) query.category = category;
            if (subCategory) query.subCategory = subCategory;
            if (active) query.visible = active;
            if (onlyMe == 'true') query.onlyMe = true;
            if (onlyMe == 'false') query.onlyMe = false;
            let ads = await Ads.find(query).populate(populateQuery)
                .sort({createdAt: -1})
                .limit(limit)
                .skip((page - 1) * limit);

            const adsCount = await Ads.count(query);
            const pageCount = Math.ceil(adsCount / limit);

            res.send(new ApiResponse(ads, page, pageCount, limit, adsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async searchWithToken(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {search,category,subCategory,country,active,onlyMe} = req.query;
            
            let query = {
                $and: [
                    { $or: [
                        {title: { $regex: '.*' + search + '.*' , '$options' : 'i'  }}, 
                      
                      ] 
                    },
                    {deleted: false},
                    {onlyMe:false}
                ]
            };
            if (country) query.country = country;
            if (category) query.category = category;
            if (subCategory) query.subCategory = subCategory;
            if (active) query.visible = active;
            if (onlyMe == 'true') query.onlyMe = true;
            if (onlyMe == 'false') query.onlyMe = false;
            let ads = await Ads.find(query).populate(populateQuery)
                .sort({createdAt: -1})
                .limit(limit)
                .skip((page - 1) * limit);
	        let theUser = await checkExistThenGet(req.user._id,User)
            for (let add of ads) {
                let theAds = await checkExistThenGet(add._id,Ads)
                theAds.viewParticipates= [...theAds.participate.slice(0,5)]
                let arr = theUser.favourite
                var found = arr.find(function(element) {
                    return element == add._id;
                }); 
                if(found){
                    theAds.isFavourite =true
                }else{
                    theAds.isFavourite =false
                }
                await theAds.save()
            }
            ads = await Ads.find(query).populate(populateQuery)
                .sort({createdAt: -1})
                .limit(limit)
                .skip((page - 1) * limit);
            const adsCount = await Ads.count(query);
            const pageCount = Math.ceil(adsCount / limit);

            res.send(new ApiResponse(ads, page, pageCount, limit, adsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async shareInside(req, res, next) {
        try {
            let {adsId} =req.params;
            let participate = req.body.participate;
            let users = await User.find({_id:participate,notif:true});
            users.forEach(user => {
                sendNotifiAndPushNotifi({
                    targetUser: user.id, 
                    fromUser: req.user._id, 
                    text: 'new notification',
                    subject: user.id,
                    subjectType: req.user.username + ' share advertisement'
                });
                let notif = {
                    "description":req.user.username + ' share new advertisement',
                    "arabicDescription":req.user.username + " تم مشاركه اعلان بواسطه" 
                }
                Notif.create({...notif,resource:req.user._id,target:user.id,ads:adsId});
            });
            let ads = await checkExistThenGet(adsId,Ads);
            ads.invitationCount = ads.invitationCount + participate.length;
            await ads.save();
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },
    shareValidateBody(isUpdate = false) {
        let validations = [
            body('email').not().isEmpty().withMessage('email is required'),
            body('message').not().isEmpty().withMessage('message is required')
            
        ];
        return validations;
    },
    async share(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let description = ' Someone invit you to download A3lanha';
            sendEmail(validatedBody.email, validatedBody.message,description)
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },
    async adshare(req, res, next) {
        try {
            let {adsId} = req.params;
            const validatedBody = checkValidations(req);
            let description = ' Someone invit you to download A3lanha';
            sendEmail(validatedBody.email, validatedBody.message,description)
            let ads = await checkExistThenGet(adsId,Ads);
            ads.invitationCount = ads.invitationCount + 1;
            await ads.save();
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },
    async enableNotif(req, res, next) {
        try {
            let {adsId} = req.params;
            let ads = await checkExistThenGet(adsId,Ads);
            ads.notif = true;
            await ads.save();
            res.send({ads});
        } catch (error) {
            next(error);
        }
    },
    async disableNotif(req, res, next) {
        try {
            let {adsId} = req.params;
            let ads = await checkExistThenGet(adsId,Ads);
            ads.notif = false;
            await ads.save();
            res.send({ads});
        } catch (error) {
            next(error);
        }
    },
    async soled(req, res, next) {
        try {
            let {adsId} = req.params;
            let ads = await checkExistThenGet(adsId,Ads);
            ads.soled = true;
            await ads.save();
            res.send({ads});
        } catch (error) {
            next(error);
        }
    },
    async unsoled(req, res, next) {
        try {
            let {adsId} = req.params;
            let ads = await checkExistThenGet(adsId,Ads);
            ads.soled = false;
            await ads.save();
            res.send({ads});
        } catch (error) {
            next(error);
        }
    },
};