import ApiResponse from "../../helpers/ApiResponse";
import Model from "../../models/models/models.model";
import Brand from "../../models/brand/brand.model";
import ApiError from '../../helpers/ApiError';

import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";


const populateQuery = [

    { path: 'brand', model: 'brand' },
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;

            let query = { deleted: false };
            let models = await Model.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const modelsCount = await Model.count(query);
            const pageCount = Math.ceil(modelsCount / limit);

            res.send(new ApiResponse(models, page, pageCount, limit, modelsCount, req));
        } catch (err) {
            next(err);
        }
    },
    
    async findAllWithoutPagenation(req, res, next) {
        try {
            let query = { deleted: false };
            let models = await Model.find(query).sort({ createdAt: -1 });
            res.send(models)
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
       
            body('brand').not().isEmpty().withMessage('brand is required')
            .isNumeric().withMessage('numeric value required'),
            body('modelName_en').not().isEmpty().withMessage('modelName_en is required')
                .custom(async (val, { req }) => {
                    let query = { modelName_en: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.modelId };

                    let modal = await Model.findOne(query).lean();
                    if (modal)
                        throw new Error('model duplicated name');

                    return true;
                }),
            body('modelName_ar').not().isEmpty().withMessage('modelName_ar is required')
                .custom(async (val, { req }) => {
                    let query = { modelName_ar: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.modelId };

                    let modal = await Model.findOne(query).lean();
                    if (modal)
                        throw new Error('model duplicated name');

                    return true;
                })
        ];

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            await checkExist(validatedBody.brand, Brand, { deleted: false });
            let createdModel = await Model.create({
                ...validatedBody,
            });
            let reports = {
                "action":"Create New Model",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdModel);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { modelId } = req.params;
            await checkExist(modelId, Model, { deleted: false });
            let model = await Model.findById(modelId).populate(populateQuery);
            res.send(model);
        } catch (err) {
            next(err);
        }
    },
    async findByBrand(req, res, next) {
        try {
            let { BrandId } = req.params;
            await checkExist(BrandId, Brand, { deleted: false });
            let query = {
                $and: [
                    {brand:BrandId}, 
                    {deleted: false} 
                ]
            }
            let model = await Model.find(query).populate(populateQuery);
            res.send(model);
        } catch (err) {
            next(err);
        }
    },
    

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let { modelId } = req.params;
            await checkExist(modelId, Model, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedModel = await Model.findByIdAndUpdate(modelId, {
                ...validatedBody,
            }, { new: true }).populate(populateQuery);
            let reports = {
                "action":"Update Model",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedModel);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { modelId } = req.params;

            let model = await checkExistThenGet(modelId, Model, { deleted: false });
            model.deleted = true;

            await model.save();
            let reports = {
                "action":"Delete Model",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('item deleted');

        }
        catch (err) {
            next(err);
        }
    },

};