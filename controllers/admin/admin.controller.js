import ApiError from "../../helpers/ApiError";
import User from "../../models/user/user.model";
import Category from "../../models/category/category.model";
import Anoncement from "../../models/anoncement/anoncement.model";
import Country from "../../models/country/country.model";
import City from "../../models/city/city.model";
import Contact from "../../models/contact/contact.model";
import Report from "../../models/reports/report.model";
import Ads from "../../models/ads/ads.model";
import Brand from "../../models/brand/brand.model";

const populateQuery = [
    { path: 'client', model: 'user' },
];
const action = [
    { path: 'user', model: 'user' },
]
export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {
                $and: [
                    {deleted: false},
                    {type:'CLIENT'}
                ]
            };
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(action)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },

   
    async count(req,res, next) {
        try {
            const usersCount = await User.count({deleted:false,type:'CLIENT'});
            const adminsCount = await User.count({deleted:false,type:'ADMIN'});
            const countriesCount = await Country.count({deleted:false});
            const citiesCount = await City.count({deleted:false});
            const anoncementCount = await Anoncement.count({deleted:false});
            const messages = await Contact.count({deleted:false,reply:false});
            const mainCategoryCount = await Category.count({ deleted: false ,main:true});
            const subCategoryCount = await Category.count({ deleted: false ,main:false});
            const adsCount = await Ads.count({ deleted: false});
            const brandCount = await Brand.count({ deleted: false});

            res.status(200).send({
                clients:usersCount,
                admins:adminsCount,
                countriesCount:countriesCount,
                citiesCount:citiesCount,
                anoncementCount:anoncementCount,
                mainCategoryCount:mainCategoryCount,
                subCategoryCount:subCategoryCount,
                messages:messages,
                adsCount:adsCount,
                brandCount:brandCount
            });
        } catch (err) {
            next(err);
        }
        
    },
    
}