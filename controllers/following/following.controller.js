import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Follow from "../../models/following/following.model";
import ApiError from '../../helpers/ApiError';
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const populateQuery = [
    {
        path: 'follow', model: 'user',
        populate: { path: 'country', model: 'country' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'country', model: 'country' },
    },
    
   
];
const populateQuery2 = [
    { path: 'countryId', model: 'country' },
   
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { user,following,search,alphabeticalOrder  } = req.query;
            //my follow list (user = my id)
            //who add me to follow list (following = my id)
            let query = { deleted:false };
            if(user) query.follower = user;
            let item = 'following';
            if(following){
                query.following = following;
                item = 'follower'
            }
            let sortd = { createdAt: -1 }
            if(alphabeticalOrder == "true"){
                sortd = {username: 1}
            }
            let usersIds = await Follow.find(query)
                .distinct(item)
            let query2 = {deleted:false}
            query2._id = { $in: usersIds}
            
            if(search){
                query2 = {
                    phone: { $regex: '.*' + search + '.*' , '$options' : 'i'  },
                    deleted: false,
                    _id:usersIds
                }
            }
            let users = await User.find(query2).populate(populateQuery2)
            let myUser = await checkExistThenGet(req.user._id, User)
            for (let user of users) {
                let theUser = await checkExistThenGet(user._id, User)
                let arr = myUser.following;
                var found = arr.find(function(element) {
                    return element == user._id;
                });
                if(found){
                    theUser.isFollowing = true
                } else{
                    theUser.isFollowing = false
                }
                await theUser.save()
            }
            
            users = await User.find(query2).populate(populateQuery2)
                .sort(sortd)
                .limit(limit)
                .skip((page - 1) * limit);
            
            const usersCount = await User.count(query2);
            const pageCount = Math.ceil(usersCount / limit);
            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
            
            
        } catch (err) {
            next(err);
        }
    },
    async create(req, res, next) {
        try {
            let {followingId} = req.params
            let user = await checkExistThenGet(req.user._id, User);
            console.log(user)
            if(!await Follow.findOne({ follower: req.user._id, following: followingId,deleted:false})){
                let arr = user.following;
                var found = arr.find(function(element) {
                    return element == followingId;
                }); 
                if(!found){
                    user.following.push(followingId);
                    await user.save();
                    let followUser = await checkExistThenGet(followingId, User);
                    let follow =  await Follow.create({ follower: req.user._id, following: followingId });
                    console.log(follow)
                        /*send notifs to user*/
                    sendNotifiAndPushNotifi({
                        targetUser: followingId, 
                        fromUser: req.user._id, 
                        text: 'A3lnha',
                        subject: follow.id,
                        subjectType: 'follow you',
                        info:'follows',
                    });
                    
                    let notif = {
                        "description":'someone follow you',
                        "arabicDescription":'قام شخص ما بمتابعتك',
                    }
                    Notif.create({...notif,resource:req.user._id,target:followingId,follow:follow.id});
                    
                } else{
                    return next(new ApiError(403, ('this follow is found in your list')));
                }
            }
            
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async unfollow(req, res, next) {
        try {
            let {followingId } = req.params;
            let follow = await Follow.findOne({ follower: req.user._id, following: followingId,deleted:false})
             /*check if user exist in follows list */
            if(!await Follow.findOne({ follower: req.user._id, following: followingId,deleted:false})){
                return next(new ApiError(403, ('this user is not found in your list')));
            }
            let follows = await checkExistThenGet(follow.id, Follow, { deleted: false });
            if (follows.follower != req.user._id)
                return next(new ApiError(403, ('not allowed')));
                follows.deleted = true;
            await follows.save();
             /*remove follow id from user date*/
            let user = await checkExistThenGet(req.user._id, User);
            let arr = user.following;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == followingId){
                    arr.splice(i, 1);
                }
            }
            user.following = arr;
            await user.save();
            res.send();
        } catch (error) {
            next(error)
        }
    },

}