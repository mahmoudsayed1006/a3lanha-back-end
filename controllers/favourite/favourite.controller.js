import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Favourite from "../../models/favourite/favourite.model";
import ApiError from '../../helpers/ApiError';
import Notif from "../../models/notif/notif.model";
import Ads from "../../models/ads/ads.model";

import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const populateQuery = [
    { path: 'country', model: 'country' },
    { path: 'category', model: 'category' },
    { path: 'subCategory', model: 'category' },
    { path: 'owner', model: 'user' },
    { path: 'color', model: 'color' },
    { path: 'city', model: 'city' },
    { path: 'brand', model: 'brand' },
    { path: 'modal', model: 'model' },
    { path: 'viewParticipates', model: 'user' },
    { path: 'participate', model: 'user' },

];
const populateQuery2 = [
    { path: 'countryId', model: 'country' },
   
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { userId,ads,alphabeticalOrder  } = req.query;
            //my favourite list (userId = my id)
            //who add ads to favourite list (ads = ads id)
            let query = { deleted:false };
            if(userId) query.user = userId;
            /*determine selected data */
            let item = 'ads';

            if(ads){
                query.ads = ads;
                item = 'user'
            }
            let sortd = { createdAt: -1 }
            if(alphabeticalOrder == "true"){
                sortd = {username: 1}
            }
             
            let usersIds = await Favourite.find(query)
                .distinct(item)
            let query2 = {deleted:false}
            query2._id = { $in: usersIds}
             /*search by name */
             /*get data by ids selectd */
            if(item == "ads"){
                let adss = await Ads.find(query2).populate(populateQuery)
                let myUser = await checkExistThenGet(req.user._id, User)
                for (let ads of adss) {
                    let theAds = await checkExistThenGet(ads._id, Ads)
                     /*check favourites */
                    let arr = myUser.favourite;
                    var found = arr.find(function(element) {
                        return element == ads._id;
                    });
                    if(found){
                        theAds.isFavourite = true
                    } else{
                        theAds.isFavourite = false
                    }
                    await theAds.save()
                }
                
                adss = await Ads.find(query2).populate(populateQuery)
                    .sort(sortd)
                    .limit(limit)
                    .skip((page - 1) * limit);
                
                const adssCount = await Ads.count(query2);
                const pageCount = Math.ceil(adssCount / limit);
                res.send(new ApiResponse(adss, page, pageCount, limit, adssCount, req));
            }else{
                let users = await User.find(query2).populate(populateQuery2)
                users = await User.find(query2).populate(populateQuery2)
                    .sort(sortd)
                    .limit(limit)
                    .skip((page - 1) * limit);
                
                const usersCount = await User.count(query2);
                const pageCount = Math.ceil(usersCount / limit);
                res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
            }
            
            
            
        } catch (err) {
            next(err);
        }
    },
     /*add ads to favourite list */
    async create(req, res, next) {
        try {
            let {adsId} = req.params
            await checkExist (adsId,Ads,{deleted:false})
            let user = await checkExistThenGet(req.user._id, User);
            if(!await Favourite.findOne({ user: req.user._id, ads: adsId,deleted:false})){
                let arr = user.favourite;
                var found = arr.find(function(element) {
                    return element == adsId;
                }); 
                if(!found){
                    user.favourite.push(adsId);
                    await user.save();
                    let theAds = await checkExistThenGet(adsId, Ads);
                    let favourite =  await Favourite.create({ user: req.user._id, ads: adsId });
                    console.log(favourite)
                    sendNotifiAndPushNotifi({
                        targetUser: theAds.owner, 
                        fromUser: req.user._id, 
                        text: 'A3lnha',
                        subject: favourite._id,
                        subjectType: 'Someone add your advertistement to his favourite list',
                        info:'favourite'
                    });
                    let notif = {
                        "description":'Someone add your advertistement to his favourite list',
                        "arabicDescription":'اضاف شخص ما اعلانك الى قائمته المفضله',
                       
                    }
                    Notif.create({...notif,resource:req.user._id,target:theAds.owner,favourite:favourite._id});
                } else{
                    return next(new ApiError(403, ('this ads is found in your list')));
                }
            }
            
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
     /*remove ads to favourite list */
    async unFavourite(req, res, next) {
        try {
            let {adsId } = req.params;
            let favourite = await Favourite.findOne({ user: req.user._id, ads: adsId,deleted:false})
             /*check if  */
            if(!await Favourite.findOne({ user: req.user._id, ads: adsId,deleted:false})){
                return next(new ApiError(403, ('this user is not found in your list')));
            }
            let favourites = await checkExistThenGet(favourite.id, Favourite, { deleted: false });
            if (favourites.user != req.user._id)
                return next(new ApiError(403, ('not allowed')));
                favourites.deleted = true;
            await favourites.save();
             /*remove ads id from user data*/
            let user = await checkExistThenGet(req.user._id, User);
            let arr = user.favourite;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == adsId){
                    arr.splice(i, 1);
                }
            }
            user.favourite = arr;
            await user.save();
            res.send();
        } catch (error) {
            next(error)
        }
    },

}