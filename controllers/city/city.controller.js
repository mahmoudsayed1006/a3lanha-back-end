import Country from "../../models/country/country.model";
import City from "../../models/city/city.model";
import { body } from "express-validator/check";
import { checkValidations } from "../shared/shared.controller";
import ApiError from "../../helpers/ApiError";
import { checkExist } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import { checkExistThenGet } from "../../helpers/CheckMethods";
export default {
    validateCityBody() {
        return [
            body('cityName').not().isEmpty().withMessage('cityName Required'),
            body('arabicName').not().isEmpty().withMessage('arabic city name Required')
        ];
    },
    async create(req, res, next) {
        try {
            let user = req.user, { countryId } = req.params;
            await checkExist(countryId, Country);
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            validatedBody.country = countryId;
            let city = await City.create({ ...validatedBody });
            return res.status(201).send(city);
        } catch (error) {
            next(error);
        }
    },
    async getById(req, res, next) {
        try {
            let { cityId } = req.params;
            
            await checkExist(cityId, City, { deleted: false });

            let city = await City.findById(cityId).populate('country');
            return res.send(city);
        } catch (error) {
            next(error);
        }
    },
    async update(req, res, next) {
        try {
            let user = req.user;
            let { cityId } = req.params;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            const validatedBody = checkValidations(req);


            let city = await City.findByIdAndUpdate(cityId, { ...validatedBody });
            return res.status(200).send(city);
        } catch (error) {
            next(error);
        }
    },

    async getAll(req, res, next) {
        try {
            let { countryId } = req.params;
            await checkExist(countryId, Country, { deleted: false });
            let cities = await City.find({ 'country': countryId, deleted: false })
            return res.send(cities);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let { countryId } = req.params;
           
            let page = +req.query.page || 1, limit = +req.query.limit || 20;


            let cities = await City.find({ 'country': countryId, deleted: false })
                .populate('country')
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });
            let count = await City.count({ 'country': countryId, deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(cities, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        }
    },


    async delete(req, res, next) {
        let { cityId } = req.params;
        try {
            let city = await checkExistThenGet(cityId, City);
            city.deleted = true;
            await city.save();
            res.send('deleted');

        } catch (err) {
            next(err);
        }
    },


}