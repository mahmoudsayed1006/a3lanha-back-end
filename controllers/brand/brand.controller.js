import ApiResponse from "../../helpers/ApiResponse";
import Brand from "../../models/brand/brand.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import ApiError from '../../helpers/ApiError';
 
export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let brands = await Brand.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const brandsCount = await Brand.count(query);
            const pageCount = Math.ceil(brandsCount / limit);

            res.send(new ApiResponse(brands, page, pageCount, limit, brandsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllWithoutPagenation(req, res, next) {
        try {
            let query = { deleted: false };
            let brands = await Brand.find(query).sort({ createdAt: -1 })
            res.send(brands);
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('brandName_en').not().isEmpty().withMessage('brandName_en is required')
                .custom(async (val, { req }) => {
                    let query = { brandName_en: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.brandId };

                    let brand = await Brand.findOne(query).lean();
                    if (brand)
                        throw new Error('Brand duplicated name');

                    return true;
                }),
            body('brandName_ar').not().isEmpty().withMessage('brandName_ar is required')
                .custom(async (val, { req }) => {
                    let query = { brandName_ar: val, deleted: false };

                    if (isUpdate)
                        query._id = { $ne: req.params.brandId };

                    let brand = await Brand.findOne(query).lean();
                    if (brand)
                        throw new Error('Brand duplicated name');

                    return true;
                })
        ];
        if (isUpdate)
            validations.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);

            let image = await handleImg(req);

            let createdBrand = await Brand.create({ ...validatedBody, img: image });
            let reports = {
                "action":"Create Brand",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdBrand);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { brandId } = req.params;
            await checkExist(brandId, Brand, { deleted: false });
            let Brand = await Brand.findById(brandId);
            res.send(Brand);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            
            let { brandId } = req.params;
            await checkExist(brandId, Brand, { deleted: false });

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validateBody.img = image;
            }

            let updatedBrand = await Brand.findByIdAndUpdate(brandId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Brand",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedBrand);
        }
        catch (err) {
            next(err);
        }
    }, 
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let { brandId } = req.params;

            let brand = await checkExistThenGet(brandId, Brand, { deleted: false });
            brand.deleted = true;

            await brand.save();
            let reports = {
                "action":"Delete Brand",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },

};