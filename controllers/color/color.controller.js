import ApiResponse from "../../helpers/ApiResponse";
import Color from "../../models/color/color.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import ApiError from '../../helpers/ApiError';
 
export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let Colors = await Color.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const ColorsCount = await Color.count(query);
            const pageCount = Math.ceil(ColorsCount / limit);

            res.send(new ApiResponse(Colors, page, pageCount, limit, ColorsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findAllWithoutPagenation(req, res, next) {
        try {
            let query = { deleted: false };
            let Colors = await Color.find(query).sort({ createdAt: -1 })
            res.send(Colors);
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('colorName').not().isEmpty().withMessage('color name is required'),
            body('arabicColorName').not().isEmpty().withMessage('arabic color name is required')
              
        ];
        if (isUpdate)
            validations.push([
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
            ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);

            let image = await handleImg(req);

            let createdColor = await Color.create({ ...validatedBody, img: image });
            let reports = {
                "action":"Create Color",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdColor);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { ColorId } = req.params;
            await checkExist(ColorId, Color, { deleted: false });
            let color = await Color.findById(ColorId);
            res.send(color);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            
            let { ColorId } = req.params;
            await checkExist(ColorId, Color, { deleted: false });

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validateBody.img = image;
            }

            let updatedColor = await Color.findByIdAndUpdate(ColorId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Color",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedColor);
        }
        catch (err) {
            next(err);
        }
    }, 
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let { ColorId } = req.params;

            let color = await checkExistThenGet(ColorId, Color, { deleted: false });
            color.deleted = true;

            await color.save();
            let reports = {
                "action":"Delete Color",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },

};