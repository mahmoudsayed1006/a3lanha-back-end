import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods";
import ApiResponse from "../../helpers/ApiResponse";
import User from "../../models/user/user.model";
import Block from "../../models/block/block.model";
import ApiError from '../../helpers/ApiError';
import Notif from "../../models/notif/notif.model";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import Follow from "../../models/following/following.model";

const populateQuery = [
    {
        path: 'block', model: 'user',
        populate: { path: 'country', model: 'country' },
    },
    {
        path: 'user', model: 'user',
        populate: { path: 'country', model: 'country' },
    },
    
   
];
const populateQuery2 = [
    { path: 'countryId', model: 'country' },
   
];
export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { blocked,user,search,alphabeticalOrder  } = req.query;
            //my block list (user = my id)
            //who add me to block list (blocked = my id)
            let query = { deleted:false };
            if(blocked) query.blocked = blocked;
            let item = 'user';
            if(user){
                query.user = user;
                item = 'blocked'
            }
            let sortd = { createdAt: -1 }
            if(alphabeticalOrder == "true"){
                sortd = {username: 1}
            }
            let usersIds = await Block.find(query)
                .distinct(item)
            let query2 = {deleted:false}
            query2._id = { $in: usersIds}
            
            if(search){
                query2 = {
                    phone: { $regex: '.*' + search + '.*' , '$options' : 'i'  },
                    deleted: false,
                    _id:usersIds
                }
            }
            let users = await User.find(query2).populate(populateQuery2)
            let myUser = await checkExistThenGet(req.user._id, User)
            for (let user of users) {
                let theUser = await checkExistThenGet(user._id, User)
                let arr = myUser.blocked;
                var found = arr.find(function(element) {
                    return element == user._id;
                });
                if(found){
                    theUser.isBlocked = true
                } else{
                    theUser.isBlocked = false
                }
                await theUser.save()
            }
            
            users = await User.find(query2).populate(populateQuery2)
                .sort(sortd)
                .limit(limit)
                .skip((page - 1) * limit);
            
            const usersCount = await User.count(query2);
            const pageCount = Math.ceil(usersCount / limit);
            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
            
            
        } catch (err) {
            next(err);
        }
    },
    async create(req, res, next) {
        try {
            let {blockedId} = req.params
            let user = await checkExistThenGet(req.user._id, User);
            console.log(user)
            if(!await Block.findOne({ user: req.user._id, blocked: blockedId,deleted:false})){
                let arr = user.blocked;
                var found = arr.find(function(element) {
                    return element == blockedId;
                }); 
                if(!found){
                    user.blocked.push(blockedId);
                    //await user.save();
                    let blockedUser = await checkExistThenGet(blockedId, User);
                    let found4 = user.blockedFrom.find(e => e == req.user._id)
                    if(!found4){
                        blockedUser.blockedFrom.push(req.user._id);
                    }
                    await Block.create({ user: req.user._id, blocked: blockedId });
                    /* delete follow from database */
                    let query1 = {deleted:false,follower: req.user._id, following: blockedId}
                    let query2 = {deleted:false,follower: blockedId, following: req.user._id}
                    let follow = await Follow.find({ $or: [query1, query2] })
                    for (let theFollow of follow ) {
                        theFollow.deleted = true;
                        await theFollow.save();
                    }
                    /*check if user follow blocked user and remove follow from user data*/
                    let found2 = user.following.find(e => e == blockedId)
                    if(found2){
                        let arr2 = user.following;
                        console.log(arr2);
                        for(let i = 0;i<= arr2.length;i=i+1){
                            if(arr2[i] == blockedId){
                                arr2.splice(i, 1);
                            }
                        }
                        user.following = arr2;
                       
                    }
                    await user.save();
                     /*check if blocked user follow user and remove follow from blocked user data*/

                     let found3 = blockedUser.following.find(e => e == req.user._id)
                     if(found3){
                         let arr3 = blockedUser.following;
                         console.log(arr3);
                         for(let i = 0;i<= arr3.length;i=i+1){
                             if(arr3[i] == req.user._id){
                                 arr3.splice(i, 1);
                             }
                         }
                         blockedUser.following = arr3;
                        
                     }
                     await blockedUser.save();
                   /*remove notifs between two users */
                   let notifs = await Notif.find({target:req.user._id,resource:blockedId})
                   for (let notif of notifs) {
                       notif.deleted = true
                       await notif.save();
                   }
                    
                } else{
                    return next(new ApiError(403, ('this block is found in your list')));
                }
            }
            
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async unblock(req, res, next) {
        try {
            let {blockedId } = req.params;
            let block = await Block.findOne({ user: req.user._id, blocked: blockedId,deleted:false})
             /*check if user exist in blocks list */
            if(!await Block.findOne({ user: req.user._id, blocked: blockedId,deleted:false})){
                return next(new ApiError(403, ('this user is not found in your list')));
            }
            let blocks = await checkExistThenGet(block.id, Block, { deleted: false });
            if (blocks.user != req.user._id)
                return next(new ApiError(403, ('not allowed')));
                blocks.deleted = true;
            await blocks.save();
             /*remove blocked id from user date*/
            let user = await checkExistThenGet(req.user._id, User);
            let arr = user.blocked;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == blockedId){
                    arr.splice(i, 1);
                }
            }
            user.blocked = arr;
            await user.save();
            /*remove user id from blocked user blockFrom array*/
            let blookUser = await checkExistThenGet(blockedId, User);
            let arr1 = blookUser.blockedFrom;
            for(let i = 0;i<= arr1.length;i=i+1){
                if(arr1[i] == req.user._id){
                    arr1.splice(i, 1);
                }
            }
            blookUser.blockedFrom = arr1;
            await blookUser.save();
            res.send();
        } catch (error) {
            next(error)
        }
    },

}