import Country from "../../models/country/country.model";
import City from "../../models/city/city.model";
import ApiError from "../../helpers/ApiError";
import ApiResponse from "../../helpers/ApiResponse";
import { body } from "express-validator/check";
import { checkExistThenGet } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";

export default {
    validateCountryBody(isUpdate = false) {
        let validations = [
            body('countryName').not().isEmpty().withMessage('countryName Required')
                .custom(async (value, { req }) => {
                    let userQuery = { countryName: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.countryId };
                    if (await Country.findOne(userQuery))
                        throw new Error('countryName duplicated');
                    else
                        return true;
                }),
                body('arabicName').not().isEmpty().withMessage('country arabic name Required')
                .custom(async (value, { req }) => {
                    let userQuery = { arabicName: value, deleted: false };
                    if (isUpdate)
                        userQuery._id = { $ne: req.params.countryId };
                    if (await Country.findOne(userQuery))
                        throw new Error('arabic country name duplicated');
                    else
                        return true;
                }),
                body('currency'),
                body('countryCode')
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);
        return validations;
    },
    async create(req, res, next) {
        try {
            let user = req.user;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin auth')));

            const validatedBody = checkValidations(req);
            let image = await handleImg(req);
            let country = await Country.create({ ...validatedBody,img:image });
            return res.status(201).send(country);
        } catch (error) {
            next(error);
        }
    },
    async getAll(req, res, next) {
        try {
            let countries = await Country.find({deleted: false }).sort({_id:-1})
            return res.status(200).send(countries);
        } catch (error) {
            next(error);
        }
    },

    async getAllPaginated(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let countries = await Country.find({ deleted: false })
                .limit(limit)
                .skip((page - 1) * limit).sort({ _id: -1 });

            let count = await Country.count({ deleted: false });

            const pageCount = Math.ceil(count / limit);

            res.send(new ApiResponse(countries, page, pageCount, limit, count, req));
        } catch (error) {
            next(error);
        } 
    }, 

    async update(req, res, next) {
        try {
            let user = req.user;
            let { countryId } = req.params;

            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let country  = await Country.findByIdAndUpdate(countryId, {
                ...validatedBody,
            }, { new: true });
            
            return res.status(200).send(country);
        } catch (error) {
            next(error);
        }
    },

    async getById(req, res, next) {
        try {
            let user = req.user;
            let { countryId } = req.params;

            let country = await checkExistThenGet(countryId, Country, { deleted: false });
            return res.send(country);
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        let { countryId } = req.params;

        try {
            let country = await checkExistThenGet(countryId, Country, { deleted: false });
            let cities = await City.find({ country: countryId });
            for (let cityId of cities) {
                cityId.deleted = true;
                await cityId.save();
            }
            country.deleted = true;
            await country.save();
            res.send('deleted');

        } catch (err) {
            next(err);
        }
    }
}