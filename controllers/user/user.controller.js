import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import { body } from 'express-validator/check';
import { checkValidations, handleImg } from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { sendForgetPassword } from '../../services/message-service';
import { sendEmail } from "../../services/emailMessage.service";
import { toImgUrl } from "../../utils";
import { generateVerifyCode } from '../../services/generator-code-service';
import Notif from "../../models/notif/notif.model";
import Block from "../../models/block/block.model";
import Suggestion from "../../models/suggestion/suggestion.model";
import Follow from "../../models/following/following.model";
import Message from "../../models/message/message.model";
import Problem from "../../models/problem/problem.model";
import Favourite from "../../models/favourite/favourite.model";
import Rate from "../../models/rate/rate.model";


import ApiResponse from "../../helpers/ApiResponse";
import bcrypt from 'bcryptjs';
const iplocate = require("node-iplocate");
import Ads from "../../models/ads/ads.model";

import { sendNotifiAndPushNotifi } from "../../services/notification-service";
const populateQuery = [ 
    { path: 'countryId', model: 'country' },
];
const checkUserExistByPhone = async (phone) => {
    let user = await User.findOne({ phone });
    if (!user)
        throw new ApiError.BadRequest('Phone Not Found');

    return user;
}
const checkUserExistByEmail = async (email) => {
    let user = await User.findOne({ email });
    if (!user)
        throw new ApiError.BadRequest('email Not Found');

    return user;
}

export default {
    async addToken(req,res,next){
        try{
            let user = req.user;
            let users = await checkExistThenGet(user.id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.token;
            });
            if(!found){
                users.token.push(req.body.token);
                await users.save();
            console.log(req.body.token);
            }
            res.status(200).send({
                users,
            });
            
        } catch(err){
            next(err);
        }
    },
    async signIn(req, res, next) {
        try{
            
            let user = req.user;
            user = await User.findById(user.id).populate(populateQuery);
            if(!user)
                return next(new ApiError(403, ('email or password incorrect')));
            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
            if(user.block == true){
                return next(new ApiError(500, ('sorry you are blocked')));
            }
            if(user.deleted == true){
                return next(new ApiError(500, ('sorry you are deleted')));
            }
            if(user.accountType =="ACTIVE"){
                let reports = {
                    "action":"User Login",
                };
    
                let report = await Report.create({...reports, user: user.id });
                res.status(200).send({
                    user: await User.findById(user.id).populate(populateQuery),
                    token: generateToken(user.id)
                });
            }else{
                res.status(200).send({
                    user: await User.findById(user.id).populate(populateQuery),
                });
            }
            
            
           
        } catch(err){
            next(err);
        }
    },

    validateFirstStepBody(isUpdate = false) {
        let validations = [
            body('signUpFrom').optional(),
            body('token').optional(),
            body('password').optional(),
            body('username').optional(),
            body('img').optional(),
            body('email').not().isEmpty().withMessage((value, { req}) => {
                return req.__('email.required', { value});
            }),
            
           
        ];
        return validations;
    },
    async LoginFirstStep(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let user = await User.findOne({email:validatedBody.email})
            if(user){
                if(user.block == true){
                    return next(new ApiError(500, ('sorry you are blocked')));
                }
                if(user.deleted == true){
                    return next(new ApiError(500, ('sorry you are deleted')));
                }
                if(req.body.token != null && req.body.token !=""){
                    let arr = user.token; 
                    var found = arr.find(function(element) {
                        return element == req.body.token;
                    });
                    if(!found){
                        user.token.push(req.body.token);
                        
                    }
                }
                await user.save();
                if(user.accountType =="ACTIVE"){
                    let reports = {
                        "action":"User Login",
                    };
        
                    let report = await Report.create({...reports, user: user.id });
                    res.status(200).send({
                        user: await User.findById(user.id).populate(populateQuery),
                        token: generateToken(user.id)
                    });
                }else{
                    res.status(200).send({
                        user: await User.findById(user.id).populate(populateQuery),
                    });
                }
                
            }else{
                validatedBody.type = 'CLIENT'
                validatedBody.accountType ="SIGNUP-PROCESS"
                let createdUser = await User.create({
                    ...validatedBody
                });
                //send code
                let theUser = await checkExistThenGet(createdUser.id,User)
                theUser.verifycode = generateVerifyCode();
                //send code
                let text = theUser.verifycode.toString();
                let description = 'A3lanha verfication code';
                sendEmail(validatedBody.email, text,description)
    
                if(req.body.token != null && req.body.token !=""){
                    let arr = theUser.token; 
                    var found = arr.find(function(element) {
                        return element == req.body.token;
                    });
                    if(!found){
                        theUser.token.push(req.body.token);
                        
                    }
                }
                await theUser.save();
                let reports = {
                    "action":"User Sign Up",
                };
                let report = await Report.create({...reports, user: createdUser.id });
                res.status(200).send({
                    user: await User.findById(createdUser.id).populate(populateQuery),
                });
    
            }
           
        } catch (err) {
            next(err);
        }
    },
    validateComplateDataBody(isUpdate = false) {
        let validations = [
            body('username').optional(),
            body('country').optional(),
            body('language').optional(),
            body('password').optional(),
            body('countryId').optional(),
            body('phone').optional()
            .custom(async (value, { req }) => {
                let {userId} = req.params;
                let user = await checkExistThenGet(userId, User);
                let userQuery = { phone: value/*,deleted:false*/ };
                if (isUpdate && user.phone === value)
                    userQuery._id = { $ne: userId };
                
                if (await User.findOne(userQuery))
                    throw new Error(req.__('phone duplicated'));
                else
                    return true;
            }),
            body('email').optional()
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { email: value/*,deleted:false*/ };
                    if (isUpdate && user.email === value)
                        userQuery._id = { $ne: userId };
                    
                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
            }),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },
    async completeSignUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let {userId} = req.params;
            let user = await checkExistThenGet(userId, User);
            user.accountType='ACTIVE'
            user.active = true;
            if(validatedBody.email){
                user.email = validatedBody.email;
            }
            if(validatedBody.phone){
                user.phone = validatedBody.phone;
            }
            if(validatedBody.username){
                user.username = validatedBody.username;
            }
            if(validatedBody.email){
                user.email = validatedBody.email;
            }
            if(validatedBody.password){
                user.password = validatedBody.password;
            }
            if(validatedBody.countryId){
                user.countryId = validatedBody.countryId;
            }
            if(validatedBody.country){
                user.country = validatedBody.country;
            }
            if (req.file) {
                let image = await handleImg(req)
                user.img = image;
            }
            await user.save();
           
            res.status(200).send({
                user: await User.findById(userId).populate(populateQuery),
                token:generateToken(userId)
            });


        } catch (error) {
            next(error);
        }
    },
    

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('username').not().isEmpty().withMessage('username is required'),
            body('country').not().isEmpty().withMessage('country is required'),
            body('countryId').optional(),
            body('language').optional(),
            body('signUpFrom').optional(),

            body('phone').not().isEmpty().withMessage('phone is required')
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('email').not().isEmpty().withMessage('email is required')
                
                .custom(async (value, { req }) => {
                    let userQuery = { email: value };
                    if (isUpdate && req.user.email === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
                }),

                body('type').not().isEmpty().withMessage('type is required')
                .isIn(['CLIENT','ADMIN']).withMessage('wrong type'),
                body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img'),

        ];
        if (!isUpdate) {
            validations.push([
                body('password').not().isEmpty().withMessage('password is required')
            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            
            if (req.file) {
               let image = await handleImg(req)
               validatedBody.img = image;
            }
            let createdUser = await User.create({
                ...validatedBody,token:req.body.token
            });
            let userr = await checkUserExistByEmail(validatedBody.email);
            userr.verifycode = generateVerifyCode(); 
            await userr.save();
            //send code
            let text = userr.verifycode.toString();
            let description = 'A3lanha verfication code';
            sendEmail(validatedBody.email, text,description)
            let reports = {
                "action":"User Sign Up",
            };
            let report = await Report.create({...reports, user: createdUser.id });

            res.status(201).send({
                user: await User.findById(createdUser.id).populate(populateQuery),
                token: generateToken(createdUser.id)
            });
            
        } catch (err) {
            next(err);
        }
    },
    
    async block(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = true;
            await activeUser.save();
            let reports = {
                "action":"block User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user block');
        } catch (error) {
            next(error);
        }
    },
    async unblock(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { userId} = req.params;
            let activeUser = await checkExistThenGet(userId,User);
            activeUser.block = false;
            await activeUser.save();
            let reports = {
                "action":"Active User",
            };
            let report = await Report.create({...reports, user: user });
            res.send('user active');
        } catch (error) {
            next(error);
        }
    },
    async enableNotif(req, res, next) {
        try {
            let activeUser = await checkExistThenGet(req.user._id,User);
            activeUser.notif = true;
            await activeUser.save();
            res.send({user: await User.findById(req.user._id).populate(populateQuery)});
        } catch (error) {
            next(error);
        }
    },
    async disableNotif(req, res, next) {
        try {
            let activeUser = await checkExistThenGet(req.user._id,User);
            activeUser.notif = false;
            await activeUser.save();
            res.send({user: await User.findById(req.user._id).populate(populateQuery)});
        } catch (error) {
            next(error);
        }
    },
    async enableComplaint(req, res, next) {
        try {
            let {userId} = req.params
            let user = await checkExistThenGet(userId,User);
            user.complaint = true;
            await user.save();
            res.send({user});
        } catch (error) {
            next(error);
        }
    },
    async disableComplaint(req, res, next) {
        try {
            let {userId} = req.params
            let user = await checkExistThenGet(userId,User);
            user.complaint = false;
            await user.save();
            res.send({user});
        } catch (error) {
            next(error);
        }
    },


    async findById(req, res, next) {
        try {
            let { id } = req.params;
            await checkExist(id, User, { deleted: false });

            let user = await User.findById(id);
            res.send(user);
        } catch (error) {
            next(error);
        }
    },

    async checkExistEmail(req, res, next) {
        try {
            let email = req.body.email;
            if (!email) {
                return next(new ApiError(400, 'email is required'));
            }
            let exist = await User.findOne({ email: email });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Email Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },

    async checkExistPhone(req, res, next) {
        try {
            let phone = req.body.phone;
            if (!phone) {
                return next(new ApiError(400, 'phone is required'));
            }
            let exist = await User.findOne({ phone: phone });
            let duplicated;
            if (exist == null) {
                duplicated = false;
            } else {
                duplicated = true
            }
            let reports = {
                "action":"User Check Mobile Exist Or Not",
            };
            let report = await Report.create({...reports, user: req.user });
            return res.status(200).send({ 'duplicated': duplicated });
        } catch (error) {
            next(error);
        }
    },
    validateUpdatedBody(isUpdate = true) {
        let validations = [
            body('username').not().isEmpty().withMessage('username is required'),
            body('country').not().isEmpty().withMessage('country is required'),
            body('language').optional(),
            body('countryId').optional(),
            body('phone').not().isEmpty().withMessage('phone is required'),
            body('email').optional()
                .custom(async (value, { req }) => {
                    let {userId} = req.params;
                    let user = await checkExistThenGet(userId, User);
                    let userQuery = { email: value,deleted:false };
                    if (isUpdate && user.email === value)
                        userQuery._id = { $ne: userId };
                    
                    if (await User.findOne(userQuery))
                        throw new Error(req.__('email duplicated'));
                    else
                        return true;
            }),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },
    async updateInfo(req, res, next) {
        try {
            let {userId} = req.params;
            const validatedBody = checkValidations(req);
            let user = await checkExistThenGet(userId, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            if(validatedBody.language)
                user.language = validatedBody.language;
            if(validatedBody.phone)
                user.phone = validatedBody.phone;
            if(validatedBody.country)
                user.country = validatedBody.country;
            if(validatedBody.countryId){
                user.countryId = validatedBody.countryId;
            }
            if(validatedBody.username)
                user.username = validatedBody.username;
            if(validatedBody.email)
                user.email = validatedBody.email;
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
           
            await user.save();
            let reports = {
                "action":"Update User Info",
            };
            let report = await Report.create({...reports, user: req.user});
            res.status(200).send({
                user: await User.findById(userId).populate(populateQuery)
            });

        } catch (error) {
            next(error);
        }
    }, 
    validateUpdatedPassword(isUpdate = false) {
        let validation = [
            body('newPassword').optional().not().isEmpty().withMessage('newPassword is required'),
            body('currentPassword').optional().not().isEmpty().withMessage('currentPassword is required')
           
        ];

        return validation;
    },
    async updatePassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);
            if (req.body.newPassword) {
                if (req.body.currentPassword) {
                    if (bcrypt.compareSync(req.body.currentPassword, user.password)) {
                        user.password = req.body.newPassword;
                    }
                    else {
                        res.status(400).send({
                            error: [
                                {
                                    location: 'body',
                                    param: 'currentPassword',
                                    msg: 'currentPassword is invalid'
                                }
                            ]
                        });
                    }
                }
            }
            await user.save();
            res.status(200).send({
                user: await User.findById(req.user._id).populate(populateQuery)
            });

        } catch (error) {
            next(error);
        }
    },
    async updateAvatar(req, res, next) {
        try {
            let {userId} = req.params;
            const validatedBody = {};
            let user = await checkExistThenGet(userId, User);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            if(validatedBody.img){
                user.img = validatedBody.img;
            }
           
            await user.save();
            res.status(200).send({
                user: await User.findById(userId).populate(populateQuery)
            });

        } catch (error) {
            next(error);
        }
    }, 
    
    validateSendCode() {
        return [
            body('email').not().isEmpty().withMessage('email Required')
        ];
    },
    async sendCodeToEmail(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            user.verifycode = generateVerifyCode(); 
            await user.save();
            //send code
            let text = user.verifycode.toString();
            let description = 'A3lanha verfication code';
            sendEmail(validatedBody.email, text,description)
            res.status(204).send();
        } catch (error) {
            next(error);
        }
    },
    validateConfirmVerifyCode() {
        return [
            body('verifycode').not().isEmpty().withMessage('verifycode Required'),
            body('email').not().isEmpty().withMessage('email Required'),
        ];
    },
    async resetPasswordConfirmVerifyCode(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode not match'));
            user.active = true;
            user.accountType ="ACTIVE"
            await user.save();
           
            res.status(204).send();
        } catch (err) {
            next(err);
        }
    },

    validateResetPassword() {
        return [
            body('email').not().isEmpty().withMessage('email is required'),
            body('newPassword').not().isEmpty().withMessage('newPassword is required')
        ];
    },

    async resetPassword(req, res, next) {
        try {

            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.email);

            user.password = validatedBody.newPassword;
            user.verifyCode = '0000';
            await user.save();
           
            res.status(204).send();

        } catch (err) {
            next(err);
        }
    },


    async updateToken(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            var found = arr.find(function(element) {
                return element == req.body.newToken;
            });
            if(!found){
                users.token.push(req.body.newToken);
                await users.save();
            }
            let oldtoken = req.body.oldToken;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == oldtoken){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async logout(req,res,next){
        try{
            let users = await checkExistThenGet(req.user._id, User);
            let arr = users.token;
            let token = req.body.token;
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == token){
                    arr.splice(arr[i], 1);
                }
            }
            users.token = arr;
            await users.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User));
        } catch(err){
            next(err)
        }
    },
    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { block,type } = req.query;
            let query = {deleted: false };

            if (block)
                query.block = true;
            if (type)
                query.type = type;
            let users = await User.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const usersCount = await User.count(query);
            const pageCount = Math.ceil(usersCount / limit);

            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let { userId } = req.params;
            let user = await checkExistThenGet(userId, User);
            user.deleted = true;
            let ads = await Ads.find({owner : userId });
            for (let add of ads ) {
                add.deleted = true;
                await add.save();
            }
            await user.save();
           // await User.findByIdAndDelete(userId);
            let reports = {
                "action":"Delete user",
            };
            let report = await Report.create({...reports, user: req.user });
            res.status(204).send();
        } catch (error) {
            next(error)
        }
    },
    async getUserWithToken(req, res, next) {
        try {
            let { userId } = req.params;
            let theUser = await checkExistThenGet(userId, User)
            let myUser = await checkExistThenGet(req.user._id, User)
            /*check if my user follow theuser */
            var found = myUser.following.find(e => e == userId)
            if(found){
                theUser.isFollowing = true
            } else{
                theUser.isFollowing = false
            }
            /*check if my user block theuser */
            var found2 = myUser.blocked.find(e => e == userId)
            if(found2){
                theUser.isBlocked = true
            } else{
                theUser.isBlocked = false
            }
             /*check if my user blocked from theuser */
             var found3 = theUser.blocked.find(e => e == req.user._id)
             if(found3){
                theUser.isBlockedFrom = true
             } else{
                theUser.isBlockedFrom = false
             }
             await theUser.save();
            res.send(
                await User.findById(userId).populate(populateQuery)
            );
        } catch (error) {
            next(error)
        }
    },
    async getUser(req, res, next) {
        try {
            let { userId } = req.params;
            let theUser = await checkExistThenGet(userId, User)
            theUser.isBlocked = false
            theUser.isFollowing = false
            theUser.isBlockedFrom = false
            await theUser.save()
            res.send(
                await User.findById(userId).populate(populateQuery)
            );
        } catch (error) {
            next(error)
        }
    },
    async searchUser (req,res,next){
        try{
            let query = {
                $and: [
                    { $or: [
                        {username: { $regex: '.*' + req.body.search + '.*' , '$options' : 'i'  }},
                        {email:{ $regex: '.*' + req.body.search + '.*' , '$options' : 'i'  }},
                      
                      ] 
                    },
                    {deleted: false} 
                ]
            };
           console.log(query);
            let result = await User.find(query)
            console.log(result);
            res.status(200).send(result)
        }
        catch(err){
            next(err);
        }
    },
    async googleLogin(req, res, next) {
        try{
            let user = await User.findOne({email:req.body.email}).populate(populateQuery);
            console.log(user)
            if(!user){
                user = await User.create({
                    socialId: req.body.socialId,
                    username: req.body.username,
                    email: req.body.email,
                    img: req.body.img,
                    signUpFrom:req.body.signUpFrom,
                    countryId:req.body.countryID,
                    country:req.body.country,
                    type:'CLIENT'
                 }); 
            }
                
            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
            res.status(200).send({
                user: await User.findOne({email:req.body.email}).populate(populateQuery),
                
                token: generateToken(user.id)
            });
            
        } catch(err){
            next(err);
        }
    },
    async socialLogin(req, res, next) {
        try{
            let user = req.user;
            user = await User.findOne({socialId:user.socialId}).populate(populateQuery);
            console.log(user)
                
            if(req.body.token != null && req.body.token !=""){
                let arr = user.token; 
                var found = arr.find(function(element) {
                    return element == req.body.token;
                });
                if(!found){
                    user.token.push(req.body.token);
                    await user.save();
                }
            }
            res.status(200).send({
                user,
                token: generateToken(user.id)
            });
            
        } catch(err){
            next(err);
        }
    },
    
    async blockPeople(req, res, next) {
        try {
            let { userId } = req.params;
            await checkExist(userId, User, { deleted: false });
            let user = await checkExistThenGet(req.user._id, User);
           
            let arr = user.blocked;
            var found = arr.find(function(element) {
                return element == userId;
            });
            if(!found){
                user.blocked.push(userId);
                await user.save();
                let blockuser = await checkExistThenGet(userId, User);
                blockuser.blockedFrom.push(req.user._id);
                await blockuser.save();
            }
            user = await User.findById(user.id).populate(populateQuery);

            res.status(200).send({
                user
            });
        } catch (error) {
            next(error)
        }
    },
    async unblockPeople(req, res, next) {
        try {
            let { userId } = req.params;
           // await checkExist(userId, User, { deleted: false });
            let user = await checkExistThenGet(req.user._id, User);
            let arr = user.blocked;
            console.log(userId);
            console.log(arr);
            for(let i = 0;i<= arr.length;i=i+1){
                if(arr[i] == userId){
                    arr.splice(i, 1);
                }
            }
            console.log(arr)
            user.blocked = arr;
            await user.save();

            let blockeuser = await checkExistThenGet(userId, User);
            let arr2 = blockeuser.blockedFrom;
            console.log(userId);
            console.log(arr2);
            for(let i = 0;i<= arr2.length;i=i+1){
                if(arr2[i] == req.user._id){
                    arr2.splice(i, 1);
                }
            }
            blockeuser.blockedFrom = arr2;
            await blockeuser.save();
            user = await User.findById(user.id).populate(populateQuery);

            res.status(200).send({
                user
            });
        } catch (error) {
            next(error)
        }
    },
    async countryName(req, res, next) {
        try{
            let countryName;
            let ipAddress = req.body.ipAddress;
            iplocate(ipAddress).then(function(results) {
                countryName = results.country;
                console.log(countryName)
                console.log("IP Address: " + results.ip);
                console.log("Country: " + results.country + " (" + results.country_code + ")"); 
                console.log("Continent: " + results.continent); 
            
            res.status(200).send({countryName});
        })
        } catch(err){
            next(err);
        }
    },
    //update email
    validateCheckPassword() {
        return [
            body('password').not().isEmpty().withMessage('password is required')
        ];
    },
    async checkPassword(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User);
                if (req.body.password) {
                    if (bcrypt.compareSync(req.body.password, user.password)) {
                        res.status(200).send({
                            success:true,
                            user: await User.findById(req.user._id).populate(populateQuery)
                        });
                    }
                    else {
                        res.status(400).send({errors: 'password invalid' });
                    }
                }
            

        } catch (error) {
            next(error);
        }
    },
    validateSendCodeUpdateEmail() {
        return [
            body('currentEmail').not().isEmpty().withMessage('currentEmail is required'),
            body('newEmail').not().isEmpty().withMessage('newEmail required')
        ];
    },
    async sendCode(req, res, next) {
        try {
            
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.currentEmail)
            console.log(user)
            let code =  generateVerifyCode(); 
            if(code.toString().length < 4){
                code = generateVerifyCode(); 
            }else{
                user.verifycode = code
            }
           // user.verifycode = '0000';
            await user.save();
            let description = 'A3lnha Confirm Code';
            sendEmail(validatedBody.newEmail, code,description)
            res.status(200).send();
        } catch (error) {
            next(error);
        }
    },
    validateUpdateEmail() {
        return [
            body('verifycode').not().isEmpty().withMessage('verifycode.required'),
            body('currentEmail').not().isEmpty().withMessage('currentEmail is required'),
            body('newEmail').not().isEmpty().withMessage('newEmail required')
        ];
    },
    async updateEmail(req, res, next) {
        try {
            let validatedBody = checkValidations(req);
            let user = await checkUserExistByEmail(validatedBody.currentEmail);
            if (user.verifycode != validatedBody.verifycode)
                return next(new ApiError.BadRequest('verifyCode notMatch'));
            user.email = validatedBody.newEmail;
            let description = 'A3lnha Update Your Email';
            sendEmail(validatedBody.newEmail, "Congratulation ",description)
            await user.save();
            res.status(200).send();

        } catch (err) {
            next(err);
        }
    },
    async delete(req, res, next) {
        try {
            let {userId } = req.params;
            //if(req.user.type != 'ADMIN')
            //return next(new ApiError(403,'admin.auth')); 
            let user = await checkExistThenGet(userId, User,
                {deleted: false });
            //remove follows
            let follows = await Follow.find({$or: [{fllower:userId},{following:userId}]});
            for (let id of follows) {//following array in user must be modify
                id.deleted = true;
                await id.save();
            }
            //remove blocks
            let blocks = await Block.find({$or: [{user:userId},{blocked:userId}]});
            for (let id of blocks) {
                id.deleted = true;
                await id.save();
            }
            //remove ads
            let ads = await Ads.find({deleted: false,owner: userId});
            for (let id of ads) {
                //remove notifs
                let notifs = await Notif.find({deleted: false,ads: id});
                for (let id2 of notifs) {
                    id2.deleted = true;
                    await id2.save();
                }
                //remove favs
                let favs = await Favourite.find({deleted: false,ads: id});
                for (let id3 of favs) {
                    id3.deleted = true;
                    await id3.save();
                }
                //remove rates
                let rates = await Rate.find({deleted: false,ads: id});
                for (let id4 of rates) {
                    id4.deleted = true;
                    await id4.save();
                }
                //remove problems
                let problems = await Problem.find({deleted: false,ads: id});
                for (let id5 of problems) {
                    id5.deleted = true;
                    await id5.save();
                }
                id.deleted = true;
                await id.save();

            }
            //remove messages
            let msgs = await Message.find({$or: [{to:userId},{from:userId}]});
            for (let id of msgs) {
                id.deleted = true;
                await id.save();
            }
            //remove notifs
            let notifs = await Notif.find({$or: [{resource:userId},{target:userId}]});
            for (let id of notifs) {
                id.deleted = true;
                await id.save();
            }
            //remove favs
            let favs = await Favourite.find({deleted: false,user: userId});
            for (let id of favs) {
                id.deleted = true;
                await id.save();
            }
            //remove rates
            let rates = await Rate.find({deleted: false,user: userId});
            for (let id of rates) {
                id.deleted = true;
                await id.save();
            }
            //remove reportes
            let reportes = await Report.find({deleted: false,user: userId});
            for (let id of reportes) {
                id.deleted = true;
                await id.save();
            }
            //remove problems
            let problems = await Problem.find({deleted: false,user: userId});
            for (let id of problems) {
                id.deleted = true;
                await id.save();
            }
            //remove suggestions
            let suggestions = await Suggestion.find({deleted: false,user: userId});
            for (let id of suggestions) {
                id.deleted = true;
                await id.save();
            }
            
            user.deleted = true
            await user.save();
            let reports = {
                "action":"Delete user",
            };
            await Report.create({...reports, user: req.user._id });
            res.status(204).send();
        }
        catch (err) {
            next(err);
        }
    },


};
