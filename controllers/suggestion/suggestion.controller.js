import ApiResponse from "../../helpers/ApiResponse";
import Suggestion from "../../models/suggestion/suggestion.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import Notif from "../../models/notif/notif.model"
import User from "../../models/user/user.model";

import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImgs, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
const populateQuery = [
    { path: 'user', model: 'user' },
   
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20 ,
            { user} = req.query
            , query = {deleted: false };

            if (user) query.user = user;

            let suggestions = await Suggestion.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const suggestionsCount = await Suggestion.count(query);
            const pageCount = Math.ceil(suggestionsCount / limit);

            res.send(new ApiResponse(suggestions, page, pageCount, limit, suggestionsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('description').not().isEmpty().withMessage('description is required'),
            

        ];
      
        return validations;
    },

    async create(req, res, next) {

        try {
            //let user = req.user;    
            const validatedBody = checkValidations(req);

            //validatedBody.user = req.user._id;
            let createdsuggestion = await Suggestion.create({ ...validatedBody});
            res.status(201).send(createdsuggestion);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
           
            let { SuggestionId } = req.params;
            await checkExist(SuggestionId, Suggestion, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedsuggestion = await Suggestion.findByIdAndUpdate(suggestionId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update suggestion",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedsuggestion);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) { 
        try {
            let user = req.user;
            
            let { SuggestionId } = req.params;
            let suggestion = await checkExistThenGet(SuggestionId, Suggestion, { deleted: false });
            suggestion.deleted = true;
            await suggestion.save();
            let reports = {
                "action":"Delete suggestion",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    
};