import ApiResponse from "../../helpers/ApiResponse";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Anoncement from "../../models/anoncement/anoncement.model";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {top} = req.query;
            let query = {deleted: false };
            if (top) query.top = top;
            let anoncements = await Anoncement.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const anoncementsCount = await Anoncement.count(query);
            const pageCount = Math.ceil(anoncementsCount / limit);

            res.send(new ApiResponse(anoncements, page, pageCount, limit, anoncementsCount, req));
        } catch (err) {
            next(err);
        }
    },
    async findSelection(req, res, next) {
        try {
            let query = { deleted: false };
            let anoncements = await Anoncement.find(query)
                .sort({ createdAt: -1 });
            res.send(ads)
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('link').not().isEmpty().withMessage('link is required'),
        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
    
            const validatedBody = checkValidations(req);
            console.log(validatedBody);
            let image = await handleImg(req);
            console.log(image);
             console.log(image);
            let createdAnoncement = await Anoncement.create({ ...validatedBody,img:image});

            let reports = {
                "action":"Create Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdAnoncement);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { anonId } = req.params;
            await checkExist(anonId, Anoncement, { deleted: false });
            let anon = await Anoncement.findById(anonId);
            res.send(anon);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { anonId } = req.params;
            await checkExist(anonId, Anoncement, { deleted: false });

            const validatedBody = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'img', isUpdate: true });
                validatedBody.img = image;
            }
            let updatedAnoncement = await Anoncement.findByIdAndUpdate(anonId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Ads",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedAnoncement);
        }
        catch (err) {
            next(err);
        }
    },
    
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { anonId } = req.params;
            let anoncement = await checkExistThenGet(anonId, Anoncement, { deleted: false });
            
            anoncement.deleted = true;
            await anoncement.save();
            let reports = {
                "action":"Delete Anoncement",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};