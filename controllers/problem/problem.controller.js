import ApiResponse from "../../helpers/ApiResponse";
import Problem from "../../models/problem/problem.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import Notif from "../../models/notif/notif.model"
import User from "../../models/user/user.model";

import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImgs, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
const populateQuery = [
    { path: 'user', model: 'user' },
    {
        path: 'ads', model: 'ads',
        populate: {  path: 'country', model: 'country'}
    },
    {
        path: 'ads', model: 'ads',
        populate: {  path: 'category', model: 'category' }
    },
    {
        path: 'ads', model: 'ads',
        populate: {  path: 'subCategory', model: 'category' }
    },
    {
        path: 'ads', model: 'ads',
        populate: { path: 'owner', model: 'user' }
    },
    {
        path: 'ads', model: 'ads',
        populate: {  path: 'color', model: 'color' }
    },
   
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20 ,
            { user,ads } = req.query
            , query = {deleted: false };

            if (user) query.user = user;
            if (ads) query.ads = ads;

            let Problems = await Problem.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const ProblemsCount = await Problem.count(query);
            const pageCount = Math.ceil(ProblemsCount / limit);

            res.send(new ApiResponse(Problems, page, pageCount, limit, ProblemsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('problemType').not().isEmpty().withMessage('problem Type is required'),
            body('description').not().isEmpty().withMessage('description is required'),
            body('ads').optional().isNumeric().withMessage('ads should be a number'),
            

        ];
        if (isUpdate)
        validations.push([
            body('img').optional().custom(val => isImgUrl(val)).withMessage('img should be a valid img')
        ]);
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;    
            const validatedBody = checkValidations(req);
            validatedBody.user = req.user._id;
            if(req.file){
                let image = await handleImgs(req);
                validatedBody.img = image
            }
            if(req.user.complaint == false)
            return next(new ApiError(505, ('you are not able to do this again')));
            let createdProblem = await Problem.create({ ...validatedBody});
            let reports = {
                "action":"Create Problem",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdProblem);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {

        try {
            let user = req.user;
           
            let { ProblemId } = req.params;
            await checkExist(ProblemId, Problem, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedProblem = await Problem.findByIdAndUpdate(ProblemId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Problem",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedProblem);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) { 
        try {
            let user = req.user;
            
            let { ProblemId } = req.params;
            let problem = await checkExistThenGet(ProblemId, Problem, { deleted: false });
            problem.deleted = true;
            await problem.save();
            let reports = {
                "action":"Delete Problem",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    async wrongProblem(req, res, next) { 
        try {
            let { ProblemId } = req.params;
            let problem = await checkExistThenGet(ProblemId, Problem, { deleted: false });
            await problem.save();
            let user = await checkExistThenGet(problem.user, User, { deleted: false });
            user.complaint = false;
            await user.save();
            sendNotifiAndPushNotifi({
                targetUser: problem.user, 
                fromUser: req.user._id, 
                text: 'new notification',
                subject: problem.id,
                subjectType: ' The complaint you submitted is incorrect. Receiving complaints from you has been suspended'
            });
            let notif = {
                "description":'The complaint you submitted is incorrect. Receiving complaints from you has been suspended',
                "arabicDescription":'الشكوى التى قدمتها غير صحيحه. تم ايقاف ميزه تلقى الشكاوى لديك'

            }
            Notif.create({...notif,resource:req.user._id,target:problem.user,problem:problem.id});
           
            res.status(204).send('wrong complaint');

        }
        catch (err) {
            next(err);
        }
    },
    async reply(req, res, next) { 
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { ProblemId } = req.params;
            let problem = await checkExistThenGet(ProblemId, Problem, { deleted: false });
            problem.reply = true;
            problem.replyText = req.body.replyText;
            await problem.save();
            sendNotifiAndPushNotifi({
                targetUser: problem.user, 
                fromUser: req.user._id, 
                text: 'new notification',
                subject: problem.id,
                subjectType: ' A3lnha reply on your Complaint'
            });
            let notif = {
                "description":' A3lnha reply on your Complaint',
                "arabicDescription":' قام تطبيق اعلنها بالرد على شكوتك'

            }
            Notif.create({...notif,resource:req.user._id,target:problem.user,problem:problem.id});
            let reports = {
                "action":'reply on Problem'
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
  
};