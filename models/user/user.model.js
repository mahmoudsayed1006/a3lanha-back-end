import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import bcrypt from 'bcryptjs';
import isEmail from 'validator/lib/isEmail';
import { isImgUrl } from "../../helpers/CheckMethods";

const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    username: {
        type: String,
        //required: true,
        trim: true
    },
    
    email: {
        type: String,
        trim: true,
        required: true,
    },
    phone: {
        type:[ String],
        //required: true,
        trim: true,
    },
    password: { 
        type: String,
       // required: true
    },
    socialId:{
        type: String,
    },
    signUpFrom:{
        type: String,
        enum:['NORMAL','FACEBOOK','GOOGLE','APPLE','normal'],
        default:'NORMAL',
    },
    type: {
        type: String,
        enum: ['CLIENT', 'ADMIN'],
        required:true
    },
    accountType: {
        type: String,
        enum: ['SIGNUP-PROCESS', 'ACTIVE'],
        default: 'ACTIVE',
        required:true
    },
    language:{
        type:String,
    },
    country:{
        type:String,
        //required:true
    },
    countryId:{
        type:String,
        ref:'country'
        //required:true
    },
    blocked:[{
        type:Number,
        ref:'user'
    }],
    blockedFrom:[{
        type:Number,
        ref:'user'
    }],
    isBlocked:{
        type: Boolean,
        default: false
    },
    isBlockedFrom:{
        type: Boolean,
        default: false
    },
    img: {
        type: String,
       
    },
    following:[{
        type:Number,
        ref:'user'
    }],
    isFollowing:{
        type: Boolean,
        default: false
    },
    favourite:[{
        type:Number,
        ref:'ads'
    }],
    verifycode: {
        type: Number
    },
    token:{
        type:[String],
    },
    block: {
        type: Boolean,
        default: false
    },
    notif: {
        type: Boolean,
        default: true
    },
    complaint:{
        type:Boolean,
        default:true
    },
    active:{
        type:Boolean,
        default:false
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true, discriminatorKey: 'kind' });

userSchema.pre('save', function (next) {
    const account = this;
    if (!account.isModified('password')) return next();

    const salt = bcrypt.genSaltSync();
    bcrypt.hash(account.password, salt).then(hash => {
        account.password = hash;
        next();
    }).catch(err => console.log(err));
});

userSchema.methods.isValidPassword = function (newPassword, callback) {
    let user = this;
    bcrypt.compare(newPassword, user.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });
export default mongoose.model('user', userSchema);