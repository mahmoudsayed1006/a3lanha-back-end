import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const blockSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    user: {//فاعل
        type: Number,
        ref: 'user',
        required: true
    },
    blocked: {//مفعول
        type: Number,
        ref: 'user',
        required: true
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

blockSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
blockSchema.plugin(autoIncrement.plugin, { model: 'block', startAt: 1 });

export default mongoose.model('block', blockSchema);