import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import { isImgUrl } from "../../helpers/CheckMethods";

const SuggestionSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    
    description: {
        type: String,
        required: true,
    },
    user: {
        type: Number,
        ref:'user',
        //required: true,
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { Suggestionstamps: true });

SuggestionSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
SuggestionSchema.plugin(autoIncrement.plugin, { model: 'suggestion', startAt: 1 });

export default mongoose.model('suggestion', SuggestionSchema);