import mongoose, { Schema } from "mongoose";
import { isImgUrl } from "../../helpers/CheckMethods";
import autoIncrement from 'mongoose-auto-increment';
const AdsSchema=new Schema({
    _id: {
        type: Number,
        required: true
    },
    img: [{
        type: String,
        //required: true,
    }],
    owner:{
        type: Number,
        ref: 'user'
    },
    category:{
        type: Number,
        ref: 'category'
    },
    subCategory:{
        type: Number,
        ref: 'category'
    },
    country:{
        type: Number,
        ref: 'country'
    },
    city:{
        type: Number,
        ref: 'city'
    },
    title:{
        type:String,
        required:true
    },
    price:{
        type:String,
    },
    address:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:true
    },
    participate:{
        type: [Number],
        ref: 'user'
    },
    viewParticipates:{
        type: [Number],
        ref: 'user'
    },
    top:{
        type:Boolean,
        default:false
    },
    rate:{
        type:Number,
        default:0
    },
    rateNumbers:{
        type:Number,
        default:0
    },
    ratePrecent:{
        type:Number,
        default:0
    },
    finalRate:{
        type:Number,
        default:0
    },
    star1:{
        type:Number,
        default:0
    },
    star2:{
        type:Number,
        default:0
    },
    star3:{
        type:Number,
        default:0
    },
    star4:{
        type:Number,
        default:0
    },
    star5:{
        type:Number,
        default:0
    },
    visible: {
        type: Boolean,
        default: false
    },
    onlyMe: {
        type: Boolean,
        default: false
    },
    soled: {
        type: Boolean,
        default: false
    },
    
    notif: {
        type: Boolean,
        default: true
    },
    //home
    rental:{
        type:String,
    },
    beds:{
        type:Number,
    },
    baths:{
        type:Number,
    },
    space:{
        type:Number,
    },
    priceFrom:{
        type:Number,
    },
    priceTo:{
        type:Number,
    },
    goal:{
        type:String
    },

    //car
    modal:{
        type:Number,
        ref:'model'
    },
    brand:{
        type:Number,
        ref:'brand'
    },
    color:{
        type:Number,
        ref:'color'
    },
    usage:{
        type:String,
    },
    year:{
        type:String,
    },
    mileage:{
        type:String,
    },
    //jop
    personal:{
        type:Boolean,
        default:false
    },
    salarySystem:{
        type:String,
    },
    date:{
        type:String,
    },
    companyName:{
        type:String
    },
    jopRequirements:{
        type:String,
    },
    experience:{
        type:String,
    },
    jopType:{
        type:String,
    },
    invitationCount:{
        type:Number,
        default:0,
    },
    location:{
        type:[Number],
        default:[1.0,1.1],
    },
    isFavourite:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    },

},{ timestamps: true });
AdsSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
AdsSchema.plugin(autoIncrement.plugin, { model: 'ads', startAt: 1 });

export default mongoose.model('ads', AdsSchema);