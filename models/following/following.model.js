import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const FollowSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    follower: {//فاعل
        type: Number,
        ref: 'user',
        required: true
    },
    following: {//مفعول
        type: Number,
        ref: 'user',
        required: true
    },
    deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true });

FollowSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
FollowSchema.plugin(autoIncrement.plugin, { model: 'follow', startAt: 1 });

export default mongoose.model('follow', FollowSchema);