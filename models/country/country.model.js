import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const countrySchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    countryName: {
        type: String,
        required: true,
        trim: true,
    },
    arabicName: {
        type: String,
        required: true,
        trim: true,
    },
    img:{
        type: String,
        
    },
    currency:{
        type: String,
    },
    countryCode:{
        type: String,
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

countrySchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});



countrySchema.plugin(autoIncrement.plugin, { model: 'country', startAt: 1 });

export default mongoose.model('country', countrySchema);
