
import passport from 'passport';
import passportJwt from 'passport-jwt';
import passportLocal from 'passport-local';
import config from '../config';
import moment from 'moment';
import User from '../models/user/user.model';
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const InstagramStrategy = require('passport-instagram').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

const JwtStrategy = passportJwt.Strategy;
const LocalStrategy = passportLocal.Strategy;
const { ExtractJwt } = passportJwt;
const { jwtSecret } = config;


passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret
}, (payload, done) => {
    User.findById(payload.sub).then(user => {
      
        if (!user)
            return done(null, false);

        return done(null, user)
    }).catch(err => {
        console.log('Passport Error: ', err);
        return done(null, false);
    })
}
));


passport.use(new LocalStrategy({
    usernameField: 'email'
}, (email, password, done) => {

    User.findOne({ email }).then(user => {
        if (!user)
            return done(null, false);

        // Compare Passwords 
        user.isValidPassword(password, function (err, isMatch) {
            if (err) return done(err);
            if (!isMatch) return done(null, false, { error: 'Invalid password' });

            return done(null, user);
        })

    });
}));

passport.use(
    new GoogleStrategy({
        // options for google strategy
        clientID: '320490392605-k2sga588q8nb37q0lq5htt9lujgbnocn.apps.googleusercontent.com',
        clientSecret: 'zDaCcufUDOYKLfeTlqT0RgWv',
        callbackURL: 'http://localhost:3000/api/v1/google/redirect',
        proxy: true,
        passReqToCallback: true
    }, (accessToken, refreshToken, profile, done) => {
	console.log(profile);
        // check if user already exists in our own db
        User.findOne({'socialId': profile.id}).then((currentUser) => {
            if(currentUser){
                console.log('user is: ', currentUser);
                done(null, currentUser);
            } else {
                new User({
                    socialId: profile.id,
                    username: profile.displayName,
                    img: profile._json.image.url,
                    type:'CLIENT'
                }).save().then((newUser) => {
                    console.log('created new user: ', newUser);
                    done(null, newUser);
                });
            }
        });
    })
);

passport.use(new InstagramStrategy({
    clientID: '9f1007c2115445c4aa5664b7023e5e0d',
    clientSecret: '7c37465da675404088513014c5231737',
    callbackURL: "https://a3lanha.herokuapp.com/api/v1/auth/instagram/callback"
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOne({ 'socialId': profile.id }, function(err, user) {
        console.log(profile)
        if (err) return callback(err);
        if (user) {
            console.log('user is: ', user);
            return done(null, user); // Check if user already exists
        }
        else {
            new User({
                socialId: profile.id,
                username: profile.displayName,
                img: profile._json.data.profile_picture,
                type:'CLIENT',
                signUpFrom:'instagram'
            }).save().then((newUser) => {
                console.log('created new user: ', newUser);
                done(null, newUser);
            });
          
        }
    });
  }
));
passport.use(new FacebookStrategy({
    clientID: '671942086660605',
    clientSecret:'c61c057e2f2fd7e3ce6cf1d59977f7f1',
    callbackURL: 'https://a3lanha.herokuapp.com/api/v1/auth/facebook/callback',
    profileFields: ['id', 'name','picture.type(large)', 'emails', 'displayName'],
  },
  function(accessToken, refreshToken, profile, done) {
    User.findOne({ 'socialId': profile.id }, function(err, user) {
        console.log(profile)
        if (err) return callback(err);
        if (user) {
            console.log('user is: ', user);
            return done(null, user); // Check if user already exists
        }
        else {
            new User({
                socialId: profile.id,
                username: profile.displayName,
                img: profile.photos ? profile.photos[0].value :'/img/faces/unknown-user-pic.jpg',
                email:profile.emails[0].value,
                //phone:profile.mobile_phone,
                type:'CLIENT',
                signUpFrom:'facebook'
            }).save().then((newUser) => {
                console.log('created new user: ', newUser);
                done(null, newUser);
            });
          
        }
    });
  }
));

passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((user, done) => done(null, user));
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignIn = passport.authenticate('local', { session: false });
const instaSignIn = passport.authenticate('instagram', { session: false });
const facebookSignIn = passport.authenticate('facebook', { session: false });

export {requireAuth,requireSignIn,instaSignIn};